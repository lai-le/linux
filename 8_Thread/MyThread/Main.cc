#include <iostream>
#include "Thread.hpp"
#include <unistd.h>
#include <pthread.h>
#include <vector>
#include "LockGuard.hpp"

using namespace ThreadMoudle;

// 互斥 抢票 加锁
int tickets = 10000;

// 局部锁
// void routine(ThreadData *td)
// {
//     // std::cout << td->_name << " lock address: " << td->_lock << std::endl;
//     // // std::cout << td->_name << std::endl;
//     // sleep(1);
//     while (true)
//     {
//         pthread_mutex_lock(td->_lock);
//         if (tickets > 0)
//         {
//             usleep(1000); // 1ms
//             std::cout << td->_name.c_str() << " get a ticket: " << tickets << std::endl;
//             tickets--;
//             pthread_mutex_unlock(td->_lock);
//         }
//         else
//         {
//             pthread_mutex_unlock(td->_lock);
//             break;
//         }
//     }
// }

// LockGuard LockGuard
void routine(ThreadData *td)
{
    while (true)
    {
        LockGuard lockguard(td->_lock); // 代码快结束自动析构
        if (tickets > 0)
        {
            usleep(1000); // 1ms
            std::cout << td->_name.c_str() << " get a ticket: " << tickets << std::endl;
            tickets--;
        }
        else
        {
            break;
        }
    }
}

static int threadnum = 4; // 线程个数

int main()
{
    pthread_mutex_t mutex;
    pthread_mutex_init(&mutex, nullptr);

    std::vector<Thread> threads;
    for (int i = 0; i < threadnum; i++)
    {
        std::string name = "thread-" + std::to_string(i + 1);
        ThreadData *td = new ThreadData(name, &mutex);
        Thread thread(name, routine, td);
        threads.emplace_back(name, routine, td);
    }

    for (auto &thread : threads)
    {
        thread.start();
    }

    for (auto &thread : threads)
    {
        thread.join();
    }

    pthread_mutex_destroy(&mutex);

    return 0;
}

// pthread_mutex_t gmutex = PTHREAD_MUTEX_INITIALIZER; // 静态/全局锁

// void routine(ThreadData *td)
// {
//     while (true)
//     {
//         pthread_mutex_lock(&gmutex);
//         if (tickets > 0)
//         {
//             usleep(1000); // 1ms
//             //std::cout << name << " get a ticket: " << tickets << std::endl;
//             tickets--;
//             pthread_mutex_unlock(&gmutex);
//         }
//         else
//         {
//             pthread_mutex_unlock(&gmutex);
//             break;
//         }
//     }
// }

// int main()
// {
//     Thread t1("thread-1", routine);
//     Thread t2("thread-2", routine);
//     Thread t3("thread-3", routine);
//     Thread t4("thread-4", routine);

//     t1.start();
//     t2.start();
//     t3.start();
//     t4.start();

//     t1.join();
//     t2.join();
//     t3.join();
//     t4.join();

//     return 0;
// }

// void ThreadPrint(const std::string &name)
// {
//     int cnt = 1;
//     while(true)
//     {
//         std::cout << name << "is running, cnt: " << cnt <<std::endl;
//         sleep(1);
//         cnt++;
//     }
// }

// int main()
// {
//     // 统一管理线程
//     //  ....  //

//     // Thread t("thread-1", ThreadPrint);
//     // t.start();
//     // std::cout << t.getName() << " status: " << t.status() << std::endl;
//     // sleep(10);

//     // t.stop();
//     // sleep(1);
//     // std::cout << t.getName() << " status: " << t.status() << std::endl;

//     // t.join();
//     // std::cout << "thread join..." << std::endl;

//     return 0;
// }