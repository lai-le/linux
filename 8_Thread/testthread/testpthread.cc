#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <vector>
#include <ctime>
#include <string>
#include <pthread.h>

__thread int gval = 100; // 现成的局部存储
// 线程 id
std::string PrintToHex(pthread_t tid)
{
    char buffer[64];
    snprintf(buffer, sizeof(buffer), "ox%lx", tid);
    return buffer;
}

void *threadRun(void *args)
{
    std::string name = static_cast<const char *>(args);
    while (true)
    {
        // std::cout << name << "is running, tid: " << PrintToHex(pthread_self()) << std::endl;
        std::cout << name << "is running, gval: " << gval << " &gval: " << &gval << std::endl;
       
        sleep(1);
         gval++;
    }
}

int main()
{
    pthread_t tid; // ??
    pthread_create(&tid, nullptr, threadRun, (void *)"thread-1");

    while (true)
    {
        sleep(1);
        std::cout << "new thread gval: " << gval << " &gval: " << &gval << std::endl;
        
    }
    pthread_join(tid, nullptr);

    return 0;
}

// 线程控制
// const int num = 10;

// void *threadRun(void *args)
// {
//     // pthread_detach
//     pthread_detach(pthread_self());
//     std::string name = static_cast<const char*>(args);
//     while(true)
//     {
//         std::cout << name << "is running" << std::endl;
//         sleep(3);
//         break;
//     }

//     //return args;

//     //exit(1);
//     pthread_exit(args);
// }

// std::string PrintToHex(pthread_t &tid)
// {
//     char buffer[64];
//     snprintf(buffer, sizeof(buffer), "ox%lx", tid);
//     return buffer;
// }

// int main()
// {
//     std::vector<pthread_t> tids;
//     for(int i = 0; i < num; i++)
//     {
//         pthread_t tid; // id
//         //char name[128]; // name
//         char *name = new char[128];
//         snprintf(name, 128, "thread-%d", i + 1);
//         pthread_create(&tid, nullptr, threadRun, name);

//         // 保存所有线程
//         tids.emplace_back(tid);
//     }

//     // 主线程分离新线程
//     for(auto tid : tids)
//     {
//         pthread_detach(tid);
//     }

//     // 分离之后 main做自己的事情
//     // while (true)
//     // {
//     //     //...
//     // }

//     //sleep(5);
//     // for(auto tid : tids)
//     // {
//     //     // pthread_cancel
//     //     // pthread_cancel(tid);
//     //     // std::cout << PrintToHex(tid) << " cancel...";

//     //     //std::cout << PrintToHex(tid) << "quit..." << std::endl;
//     //     void *result = nullptr;
//     //     int n = pthread_join(tid, &result);
//     //     std::cout << (long long int)result << " quit... n: " << n << std::endl;
//     //     //delete (const char*)name;
//     // }

//     sleep(100);

//     return 0;
// }

// 可以给线程传递多个参数，甚至方法了(封装在类里面)
// class ThreadData
// {
// public:
//     std::string name;
//     int num;
// };

// class ThreadResult
// {
// public:
//     int x;
//     int y;
//     int result;
// };

// // 看待线程函数的返回值：a. 一异常整个崩掉
// void *threadRun(void *args)
// {
//     // std::string name = (const char*)args;
//     ThreadData *td = static_cast<ThreadData *>(args); // 强转

//     int cnt = 10;
//     while (cnt)
//     {
//         std::cout << td->name << ", new thread run..., num " << td->num << ", cnt: " << cnt-- << std::endl;
//         // std::cout << name << "new thread run..., cnt: " << cnt-- << std::endl;
//         sleep(3);
//         // int *p = nullptr; // 野指针
//         // *p = 100;
//     }
//     delete td;

//     return (void *)111;
// }

// // 16进制打印tid
// std::string PrintToHex(pthread_t &tid)
// {
//     char buffer[64];
//     snprintf(buffer, sizeof(buffer), "ox%lx", tid);
//     return buffer;
// }

// int main()
// {
//     pthread_t tid;
//     // ThreadData td;
//     ThreadData *td = new ThreadData();
//     td->name = "thread-1";
//     td->num = 100;
//     // int n = pthread_create(&tid, nullptr, threadRun, (void*)"thread-1");
//     int n = pthread_create(&tid, nullptr, threadRun, td);
//     if (n != 0)
//     {
//         std::cerr << "create thread error" << std::endl;
//         return -1;
//     }

//     std::string tid_str = PrintToHex(tid);
//     std::cout << "tid: " << tid_str << std::endl;

//     std::cout << "main thread join begin...." << std::endl;
//     void *code = nullptr; // 开辟了空间的
//     n = pthread_join(tid, &code);
//     if (n == 0)
//     {
//         std::cout << "main thread wait sucess, new thread exit code: " << (uint64_t)code << std::endl;
//     }

//     sleep(199);

//     return 0;
// }

// test pthread
// void *StartRoutine(void *arg)
// {
//     // new thread
//     while (true)
//     {
//         int x = rand() % 5;

//         std::cout << "new thread running, pid: " << getpid() << "x" << x << std::endl;
//         sleep(1);

//         if(x == 0)
//         {
//             int *p = nullptr;
//             *p = 100; // 野指针
//         }
//     }
// }

// int main()
// {
//     srand(time(nullptr));

//     pthread_t newthread;
//     pthread_create(&newthread, nullptr, StartRoutine, (void *)"new thread");

//     pthread_t newthread1;
//     pthread_create(&newthread1, nullptr, StartRoutine, (void *)"new thread");

//     pthread_t newthread2;
//     pthread_create(&newthread2, nullptr, StartRoutine, (void *)"new thread");

//     // main thread
//     while (true)
//     {
//         sleep(1);
//         std::cout << "main thread running, pid" << getpid() << std::endl;
//     }

//     return 0;
// }