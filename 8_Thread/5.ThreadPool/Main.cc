#include <iostream>
#include <pthread.h>
#include <unistd.h>

#include "ThreadPool.hpp"
#include "Task.hpp"
#include "Log.hpp"

using namespace log_ns;

// int main()
// {
//     //Log().LogRowMessage("main.cc", 11, 1, "hello %d world %.2f\n", 1, 3.14);
    
//     //Log lg;
//     LOG(INFO, "hello world%d, %s\n", 22, "linux");

//     //lg.ShifType(FILETYPE);
//     // lg.LogRowMessage(__FILE__, __LINE__, DEBUGE, "%.2f hello world %d\n", 3.14, 1);
//     // lg.LogRowMessage(__FILE__, __LINE__, DEBUGE, "%.2f hello world %d\n", 3.14, 1);
//     // lg.LogRowMessage(__FILE__, __LINE__, DEBUGE, "%.2f hello world %d\n", 3.14, 1);
//     // lg.LogRowMessage("main.cc", 14, DEBUGE, "%.2f hello world %d\n", 3.14, 1);
//     // lg.LogRowMessage("main.cc", 14, DEBUGE, "%.2f hello world %d\n", 3.14, 1);

//     return 0;
// }

int main()
{
    // ThreadPool<Task> *tp = new ThreadPool<Task>();
    // tp->Init();
    // tp->Start();
    //sleep(10);

    int cnt = 10;
    while(cnt)
    {
        // 不断向线程池推送任务
        //sleep(1);
        Task t(1, 2);
        ThreadPool<Task>::GetInstance()->Equeue(t);
        sleep(1);
        // std::cout << "cnt: " << cnt-- << std::endl; 
        LOG(INFO, "equeue a tassk,  %s cnt: %d\n", t.debuge().c_str(), cnt);
        cnt--;
    }
    
    ThreadPool<Task>::GetInstance()->Stop();
    // std::cout << "threads stop...." << std::endl;
    LOG(INFO, "thread pool stop..\n");
    sleep(10);

    return 0;
}