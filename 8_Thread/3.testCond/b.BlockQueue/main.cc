#include "BlockingQueue.hpp"
#include <pthread.h>
#include <iostream>
#include <unistd.h>

// 单生产 单消费
void *Consumer(void *args)
{
    BlockingQueue<int> *bq = static_cast<BlockingQueue<int> *>(args);
    while(true)
    {
        //sleep(2);
        // 1. 获取数据
        int data = 0;
        bq->Pop(&data);

        // 2. 处理数据
        std::cout << "consumer -> " << data << std::endl;
    }

}

void *Productor(void *args)
{
    srand(time(nullptr) ^ getpid());
    BlockingQueue<int> *bq = static_cast<BlockingQueue<int> *>(args);
    while(true)
    {
        sleep(2);
        // 1. 构建数据
        int data = rand() % 10 + 1;
        // 2. 生产数据
        bq->Equeue(data);
        std::cout << "Productor -> " << data << std::endl;
    }
}

int main()
{
    BlockingQueue<int> *bq = new BlockingQueue<int>();
    pthread_t c, p;
    pthread_create(&c, nullptr, Consumer, bq);
    pthread_create(&p, nullptr, Productor, bq);

    pthread_join(c, nullptr);
    pthread_join(p, nullptr);

    return 0;
}