#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <ctime>
#include "RingQueue.hpp"
#include "Task.hpp"

void *Consumer(void *args)
{
    RingQueue<Task> *rq = static_cast<RingQueue<Task>*>(args);
    while(true)
    {
        //sleep(1);
        // 1. 获取数据
        Task t;
        rq->Pop(&t);
        // 2. 处理数据
        t.Excute();
        std::cout << "Consumer -> " << t.getResult() << std::endl;
    }
}

void *Productor(void *args)
{
    RingQueue<Task> *rq = static_cast<RingQueue<Task>*>(args);
    while(true)
    {
        sleep(1);
        // 1. 构建数据
        int x = rand() % 10 + 1;
        usleep(1000);
        int y = rand() % 10 + 1;
        Task t(x, y);
        // 2. 生产数据
        rq->Push(t);

        std::cout << "Productor -> " << t.debuge() << std::endl;
    }
}
int main()
{
    srand(time(nullptr) ^ getpid());
    RingQueue<Task> *rq = new RingQueue<Task>();
    pthread_t c, c2, p, p2, p3;
    pthread_create(&c, nullptr, Consumer, rq);
    pthread_create(&c2, nullptr, Consumer, rq);
    pthread_create(&p, nullptr, Productor, rq);
    pthread_create(&p2, nullptr, Productor, rq);
    //pthread_create(&p3, nullptr, Productor, rq);

    pthread_join(c, nullptr);
    pthread_join(c2, nullptr);
    pthread_join(p, nullptr);
    pthread_join(p2, nullptr);
    pthread_join(p3, nullptr);

    return 0;
}