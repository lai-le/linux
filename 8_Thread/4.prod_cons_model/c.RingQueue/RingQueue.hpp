#pragma once
#include <pthread.h>
#include <vector>
#include <semaphore.h>
#include <string>

template <class T>
class RingQueue
{
public:
    void P(sem_t &sem)
    {
        sem_wait(&sem);
    }

    void V(sem_t &sem)
    {
        sem_post(&sem);
    }

public:
    RingQueue(size_t max_cap = 5)
        : _max_cap(max_cap)
        , _ring_queue(max_cap)
        , _c_index(0) , _p_index(0)
    {
        sem_init(&_data_sem, 0, 0);
        sem_init(&_space_sem, 0, _max_cap);

        pthread_mutex_init(&_c_mutex, nullptr);
        pthread_mutex_init(&_p_mutex, nullptr);
    }

    void Pop(T *out) // 消费
    {
        P(_data_sem);
        pthread_mutex_lock(&_c_mutex);
        *out = _ring_queue[_c_index];
        _c_index++; // 多生产 多消费 -> 下标成为了 临界资源 所以必须加锁
        _c_index %= _max_cap;
        pthread_mutex_unlock(&_c_mutex);

        V(_space_sem);
    }

    void Push(const T& in)
    {
        //pthread_mutex_lock(&_p_mutex);

        P(_space_sem);
        pthread_mutex_lock(&_p_mutex);
        _ring_queue[_p_index] = in;
        _p_index++;
        _p_index %= _max_cap;
        pthread_mutex_unlock(&_p_mutex);

        V(_data_sem);

    }

    ~RingQueue()
    {
        sem_destroy(&_data_sem);
        sem_destroy(&_space_sem);

        pthread_mutex_destroy(&_c_mutex);
        pthread_mutex_destroy(&_p_mutex);
    }

private:
    std::vector<T> _ring_queue;
    size_t _max_cap;

    int _c_index;
    int _p_index;
    // 信号量
    sem_t _data_sem;  // 消费者关心
    sem_t _space_sem; // 生产者关心

    pthread_mutex_t _c_mutex;
    pthread_mutex_t _p_mutex;
};