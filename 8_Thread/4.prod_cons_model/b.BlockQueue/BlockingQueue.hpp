#include <iostream>
#include <pthread.h>
#include <string>
#include <queue>

const static size_t DEFAULT_CAP = 5;

template <typename T>
class BlockingQueue
{
private:
    bool IsFull()
    {
        return _block_queue.size() == _max_cap;
    }
    bool IsEmpty()
    {
        return _block_queue.empty();
    }

public:
    BlockingQueue(size_t cap = DEFAULT_CAP) : _max_cap(cap)
    {
        pthread_mutex_init(&_mutex, nullptr);
        pthread_cond_init(&_c_cond, nullptr);
        pthread_cond_init(&_p_cond, nullptr);
    }

    // 消费者
    void Pop(T *out)
    {
        pthread_mutex_lock(&_mutex);
        // 为空 等待
        while (IsEmpty()) // while 循环检测(特例：2个c 1个p)
        {
            pthread_cond_wait(&_c_cond, &_mutex);
        }

        // 1. 不为空 || 2. 被唤醒 消费
        *out = _block_queue.front();
        _block_queue.pop();

        pthread_mutex_unlock(&_mutex);
        // 通知生产者 生产
        pthread_cond_signal(&_p_cond);
    }

    // 生产者
    void Equeue(const T &in)
    {
        pthread_mutex_lock(&_mutex);
        while (IsFull()) // 满了 等待
        {
            pthread_cond_wait(&_p_cond, &_mutex);
        }

        // 1. 没满 || 2. 被唤醒  生产
        _block_queue.push(in);
        pthread_mutex_unlock(&_mutex);

        // 通知消费者 消费
        pthread_cond_signal(&_c_cond);
    }

    ~BlockingQueue()
    {
        pthread_mutex_destroy(&_mutex);
        pthread_cond_destroy(&_c_cond);
        pthread_cond_destroy(&_p_cond);
    }

private:
    std::queue<T> _block_queue;
    size_t _max_cap;

    pthread_mutex_t _mutex;
    pthread_cond_t _c_cond;
    pthread_cond_t _p_cond;
};