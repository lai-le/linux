#include <iostream>

class Task
{
public:
    Task(){}
    Task(int x, int y)
        : _x(x), _y(y)
    {}

    void Excute()
    {
        _result = _x + _y;
    }
    int getResult()
    {
        return _result;
    }
    std::string debuge()
    {

        std::string debuge1 = std::to_string(_x) + "+" + std::to_string(_y) + "= ?";
        return debuge1;
    }

    void operator()()
    {
        Excute();
    }

    ~Task()
    {}

private:
    int _x;
    int _y;
    int _result;
};