#include "BlockingQueue.hpp"
#include <pthread.h>
#include <iostream>
#include <unistd.h>
#include "Task.hpp"

// 单生产 单消费
void *Consumer(void *args)
{
    BlockingQueue<Task> *bq = static_cast<BlockingQueue<Task> *>(args);
    while(true)
    {
        sleep(1);
        //sleep(2);
        // 1. 获取数据
        Task t;
        // int data = 0;
        bq->Pop(&t);

        // 2. 处理数据
        t.Excute();
        std::cout << "consumer -> " << t.getResult() << std::endl;
    }

}

void *Productor(void *args)
{
    srand(time(nullptr) ^ getpid());
    BlockingQueue<Task> *bq = static_cast<BlockingQueue<Task> *>(args);
    while(true)
    {
        // 1. 构建数据
        int x = rand() % 10 + 1;
        int y = rand() % 10 + 1;
        Task t(x, y);
        // 2. 生产数据
        bq->Equeue(t);
        std::cout << "Productor -> " << t.debuge() << std::endl;
        sleep(2);
    }
}

int main()
{
    BlockingQueue<Task> *bq = new BlockingQueue<Task>();
    pthread_t c1, c2, p1, p2, p3;
    pthread_create(&c2, nullptr, Consumer, bq);
    pthread_create(&c1, nullptr, Consumer, bq);
    pthread_create(&p1, nullptr, Productor, bq);
    pthread_create(&p2, nullptr, Productor, bq);
    pthread_create(&p3, nullptr, Productor, bq);

    pthread_join(c2, nullptr);
    pthread_join(c1, nullptr);
    pthread_join(p2, nullptr);
    pthread_join(p1, nullptr);
    pthread_join(p3, nullptr);

    return 0;
}