#include "./mylib/include/mymath.h"
#include "./mylib/include/myFILE.h"
#include <stdio.h>
#include <string.h>

int main()
{
  int a = 10;
  int b = 10;

  printf("%d + %d = %d\n", a, b, myAdd(a, b));

  myFILE *fp = my_fopen("./myfile.txt", "w");
  if(fp == NULL)  return 1;

  const char* message = "this is my work...\n";

  my_write(fp, message, strlen(message));

  my_close(fp);

  return 0;
}
