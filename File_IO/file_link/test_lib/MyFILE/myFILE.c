#include "myFILE.h"

myFILE* my_fopen(const char *path, const char *way)
{
    int flag = 0;
    int iscreate = 0;
    mode_t mode = 0666;

    if(strcmp(way, "r") == 0)
    {
      flag = (O_RDONLY);
    }
    else if(strcmp(way, "w") == 0)
    {
      iscreate = 1;
      flag = (O_WRONLY | O_CREAT | O_TRUNC);
    }
    else if(strcmp(way, "a") == 0)
    {
      iscreate = 1;
      flag = (O_CREAT | O_WRONLY | O_APPEND);
    }
    else 
    {}

    int fd = 0;
    if(iscreate)
      fd = open(path, flag, mode);
    else 
      fd = open(path, flag);
    if(fd < 0) return NULL;

    myFILE *fp = (myFILE*)malloc(sizeof(myFILE));
    if(NULL == fp)  return NULL;
    fp->_fileno = fd;
    fp->_pos = 0;
    fp->_flags = FLUSH_LINE;
    fp->_capacity = LINE_SIZE;

    return fp;
}

void my_fflush(myFILE *fp)
{
  write(fp->_fileno, fp->_cache, fp->_pos);
  fp->_pos = 0;
}

ssize_t my_write(myFILE *fp, const char *data, size_t len)
{
  memcpy(fp->_cache+fp->_pos, data, len);
  fp->_pos += len;

  if((fp->_flags&FLUSH_LINE) && fp->_cache[fp->_pos - 1] == '\n')
  {
    my_fflush(fp);
  }

  return len;
}

void my_close(myFILE *fp)
{
  my_fflush(fp);
  close(fp->_fileno);
  free(fp);
}
