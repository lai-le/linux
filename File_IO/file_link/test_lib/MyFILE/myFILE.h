#pragma once
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>

#define LINE_SIZE 1024
#define FLUSH_NOW  1
#define FLUSH_LINE 2
#define FLUSH_FULL 4


struct _myFILE
{
  unsigned int _flags; //mark the flush mode
  int _fileno;
  char _cache[LINE_SIZE];
  int _pos;
  int _capacity;
};

typedef struct _myFILE myFILE; 

myFILE* my_fopen(const char *path, const char *mode);
void my_fflush(myFILE *fp);
ssize_t my_write(myFILE *fp, const char *data, size_t len);
void my_close(myFILE *fp);
