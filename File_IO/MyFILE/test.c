#include "myFILE.h"
#include <stdio.h>
#include <unistd.h>
#define FILE_NAME "log.txt"

int main()
{
  myFILE *fp = my_fopen(FILE_NAME, "w");
  if(NULL == fp)
  {
    return 1;
  }
  
  int cnt = 10;
  const char *message = "hello my_FILE!";
  char buffer[128];
  while(cnt)
  {
    sprintf(buffer, "message: %s - %d", message, cnt);
    my_write(fp, buffer, strlen(buffer));
    cnt--;
    my_fflush(fp);
    sleep(1);
  }

  my_close(fp);

  return 0;
}
