#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>


const char *filename = "log.txt";

int main()
{
  //test buffer strategy
  printf("hello printf\n");
  fprintf(stdout,"hello fprintf\n");

  const char *message = "hello write\n";
  write(1,message, strlen(message));

  fork(); 



// // int fd = open(filename, O_CREAT | O_WRONLY | O_TRUNC, 0666);
//  int fd = open("/dev/pts/1", O_CREAT | O_WRONLY | O_APPEND, 0666);
////  int fd = open(filename, O_CREAT | O_WRONLY | O_TRUNC, 0666);
//  
//  dup2(fd, 1);
//
//  printf("hello linux!\n");
//  fprintf(stdout, "hello world\n");




//  close(1);
//  int fd = open(filename, O_CREAT | O_WRONLY | O_TRUNC, 0666);
//  if(fd < 0)
//  {
//      perror("open");
//      return 1;
//  }
//
//  printf("fd: %d\n",fd);
//  fprintf(stdout,"fd: %d\n",fd);
//  fflush(stdout);
//  close(fd);
//
  return 0;
}




//int main()
//{
//  struct stat st;
//  int n = stat(filename, &st);
//  if(n < 0) return 1;
//  printf("file size: %lu\n", st.st_size);
//
//  //int fd = open(filename, O_CREAT | O_WRONLY | O_TRUNC, 0666);
//  int fd = open(filename, O_RDONLY, 0666);
//  if(fd < 0)
//  {
//    perror("open fail!\n");
//    return 1;
//  }
//
//  printf("fd: %d\n",fd);
//  
//  char *file_buffer = (char*)malloc(st.st_size + 1);
//
//  //read file, return size of file
//  n = read(fd, file_buffer, st.st_size);
//  if(n > 0)
//  {
//    //hope read a string
//    file_buffer[n] = '\0';
//    printf("%s",file_buffer);
//  }
//
//  free(file_buffer);
//  
//  //const char *message = "hello linux!\n";
//  //write(fd, message, strlen(message));
//
//  close(fd);
//  return 0;
//}
