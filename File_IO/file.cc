#include <iostream>
#include <string>
#include <fstream>

#define FILENAME "log.txt"

int main()
{
  std::ofstream out(FILENAME);
  if(!out.is_open())  return 1;

  std::string message = "hello c++!\n";
  out.write(message.c_str(),message.size());

  out.close();

  return 0;
}
