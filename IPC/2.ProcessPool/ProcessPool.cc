#include <iostream>
#include <string>
#include <vector>
#include <cstring>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include "Task.hpp"
#include <sys/wait.h>

// master
class Channel
{
public:
    Channel(int wfd, pid_t subprocessid, std::string name)
        : _wfd(wfd), _subprocessid(subprocessid), _name(name)
    {
    }

    int GetWfd() { return _wfd; }
    pid_t GetSubrocessid() { return _subprocessid; }
    std::string GetName() { return _name; }

    void CloseChannel()
    {
        close(_wfd);
    }

    void WaitPid()
    {
        pid_t wid = waitpid(_subprocessid, nullptr, 0);
        if (wid > 0)
        {
            std::cout << "wait sub process success, sub pid:" << wid << std::endl;
        }
    }

    ~Channel()
    {
    }

private:
    int _wfd;
    pid_t _subprocessid;
    std::string _name;
};

void work(int rfd)
{
    while (true)
    {
        sleep(1);
        int command = 0;
        int n = read(rfd, &command, sizeof(command));
        if (n == sizeof(int))
        {
            std::cout << "sub process: " << getpid() << " handle task..." << std::endl;
            ExcuteTask(command);
        }
        else if (n == 0)
        {
            std::cout << "sub process: " << getpid() << "quit" << std::endl;
            break;
        }
    }
}

void CreateChannelAndSub(int num, std::vector<Channel> *channels)
{
    for (int i = 0; i < num; i++)
    {
        // a. create pipe
        int pipefd[2] = {0};
        int n = pipe(pipefd);
        if (n < 0)
        {
            std::cerr << "errno: " << errno << ": "
                      << "errstring: " << strerror(errno) << std::endl;
            exit(-1);
        }

        // b. create child process
        pid_t id = fork();
        if (id == 0)
        {
            // child
            close(pipefd[1]);
            work(pipefd[0]);
            close(pipefd[1]);
            exit(0);
        }

        // c. create a channel name
        std::string channel_name = "channel-0" + std::to_string(i);

        // father
        close(pipefd[0]);
        // a. child's pid b. the "w" of pipe
        channels->push_back(Channel(pipefd[1], id, channel_name));
    }
}

int SelectChannel(int channelnum)
{
    static int next = 0;
    int channel = next;
    next++;
    next %= channelnum;
    return channel;
}

void SendTaskCommand(Channel &channel, int command_code)
{
    write(channel.GetWfd(), &command_code, sizeof(command_code));
}

void CtrlPross(std::vector<Channel> &channels)
{
    int num = 3;
     while (num--)
     {
        sleep(1);
        // a. select a task
        int command_code = SelectTask();
        // b. select a channel and a sub process
        int channel_index = SelectChannel(channels.size());
        // c. send task to sub process
        SendTaskCommand(channels[channel_index], command_code);

        std::cout << std::endl;
        std::cout << "command_code: " << command_code << "channel: " << channels[channel_index].GetName() << " sub process id: " << channels[channel_index].GetSubrocessid() << std::endl;
    }
}

void CleanUpChannel(std::vector<Channel> &channels)
{
    for (auto &channel : channels)
    {
        channel.CloseChannel();
    }

    for (auto &channel : channels)
    {
        channel.WaitPid();
    }
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cerr << "Usage command: " << argv[0] << "process's count: " << argc << std::endl;
        return 1;
    }
    int num = std::stoi(argv[1]);
    LoadTask();

    std::vector<Channel> channels; // preserve channels

    // 1. create pipe and child process
    CreateChannelAndSub(num, &channels);

    // 2. control sub process by using channels
    CtrlPross(channels);

    // 3. recycle pipe and process resources
    CleanUpChannel(channels);

    //sleep(100);

    // for test
    //  for(auto& channel : channels)
    //  {
    //      std::cout << "==================================================" << std::endl;
    //      std::cout << channel.GetName() << std::endl;
    //      std::cout << channel.GetWfd() << std::endl;
    //      std::cout << channel.GetSubrocessid() << std::endl;
    //  }

    return 0;
}