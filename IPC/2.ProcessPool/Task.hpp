#pragma once

#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#define TASKNUM 3

typedef void (*task_t)(); // fuction Pointer

void Print()
{
    std::cout << "excute print process..." << std::endl;
}
void DownLoad()
{
    std::cout << "excute download process..." << std::endl;
}
void Flush()
{
    std::cout << "excute flush process..." << std::endl;
}

task_t tasks[TASKNUM];

void LoadTask()
{
    srand(time(nullptr) ^ getpid());
    tasks[0] = Print;
    tasks[1] = DownLoad;
    tasks[2] = Flush;
}

int SelectTask()
{
    return rand() % TASKNUM;
}

void ExcuteTask(int task_code)
{
    if(task_code < 0 || task_code > 2)  
    {
        perror("task_code err!!!");
        return;
    }

    tasks[task_code]();
}