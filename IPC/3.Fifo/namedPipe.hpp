#pragma once

#include <iostream>
#include <string>
#include <sys/types.h>
#include <sys/stat.h>
#include <cstring>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>

#define CREATOR 1
#define USER 2
#define WRITE O_WRONLY
#define READ O_RDONLY
#define BASESIZE 4096
#define DEFAULTFD -1

const std::string path_name = "./myfifo";

class NamedPipe
{
public:
    NamedPipe(const std::string &path_name, int who)
        : _path_name(path_name), _who(who), _fd(DEFAULTFD)
    {
        if (_who == CREATOR)
        {
            int res = mkfifo(path_name.c_str(), 0666);
            if (res < 0)
            {
                std::cerr << "errno" << errno << ":"
                          << "err string: " << strerror(errno) << std::endl;
            }
            else
            {
                std::cout << "creator create namedpipe success!" << std::endl;
            }
            sleep(3);
        }
        else
        {
            std::cout << "user get the namedpipe." << std::endl;
        }
    }

    bool OpenNamedPipe(int mode)
    {
        _fd = open(path_name.c_str(), mode);
        if (_fd < 0)
        {
            std::cerr << "errno" << errno << ":"
                      << "err string: " << strerror(errno) << std::endl;
            return false;
        }

        return true;
    }

    bool OpenForWrite()
    {
        return OpenNamedPipe(WRITE);
    }

    bool OpenForRead()
    {
        return OpenNamedPipe(READ);
    }

    int WriteNamedPipe(std::string &in)
    {
        int n = write(_fd, in.c_str(), in.size());

        return n;
    }

    int ReadNamedPipe(std::string *out)
    {
        char buffer[BASESIZE];
        int n = read(_fd, buffer, sizeof(buffer));
        if (n > 0)
        {
            buffer[n] = 0;
            *out = buffer;
        }

        return n;
    }

    ~NamedPipe()
    {
        if (_who == CREATOR)
        {

            int res = unlink(path_name.c_str());
            if (res < 0)
            {
                std::cerr << "errno" << errno << ":"
                          << "err string: " << strerror(errno) << std::endl;
            }
            else
            {
                std::cout << "Namedpipe has been released...." << std::endl;
            }
        }
        if (_fd != DEFAULTFD)
            close(_fd);
    }

private:
    std::string _path_name;
    int _fd;
    int _who;
};
