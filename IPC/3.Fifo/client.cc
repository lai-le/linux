#include "namedPipe.hpp"

// write
int main()
{
    NamedPipe fifo(path_name, USER);
    
    if(fifo.OpenForWrite())
    {
        std::cout << "Ready to send massage." << std::endl;

        while(true)
        {
            std::cout << "Please input> " << std::endl;
            std::string message;
            std::getline(std::cin, message);
            if(message == "STOP")
            {
                break;
            }
            fifo.WriteNamedPipe(message);
        }
    }

    return 0;
}