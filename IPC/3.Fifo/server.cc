#include "namedPipe.hpp"

// read
int main()
{
    NamedPipe fifo(path_name, CREATOR);

    if (fifo.OpenForRead())
    {
        std::cout << "Ready to read." << std::endl;

        while (true)
        {
            std::cout << "catch massage: " << std::endl;
            std::string message;
            int n = fifo.ReadNamedPipe(&message);
            if (n > 0)
            {
                std::cout << "message content: " << message << std::endl;
            }
            else if(n == 0)
            {
                std::cout << "read work is ending..." << std::endl;
                break;
            }
            else
            {
                std::cerr << "errno" << errno << ":" << "err string: " << strerror(errno) << std::endl;
                break;
            }
        }
    }

    return 0;
}
