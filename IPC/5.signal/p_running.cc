#include <iostream>
#include <unistd.h>
#include <signal.h> 

void handler(int sig)
{
    std::cout << "handler sig:" << sig << std::endl;
}

int main()
{
    signal(2, handler);
    while(true)
    {
        std::cout << "hello world!!! pid: " << getpid() << std::endl;
        sleep(1);
    }

    return 0;
}