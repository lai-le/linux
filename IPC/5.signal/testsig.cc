#include <iostream>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <cstdio>

// 不想关心子进程退出信息 且 不产生僵尸
int main()
{
    signal(SIGCHLD, SIG_IGN);// 手动设置对SIGCHLD进行忽略

    pid_t id = fork();
    if (id == 0)
    {
        int cnt = 5;
        while (cnt)
        {
            std::cout << "child running" << std::endl;
            sleep(1);
            cnt--;
        }
        exit(1);
    }

    // father
    while (true)
    {
        std::cout << "father running" << std::endl;
        sleep(1);
    }
    return 0;
}

// SIGCHID
// 如果5个退出 5个永远不退出呢？
// void notice(int signo)
// {
//     std::cout << "get a signo: " << signo << " pid: " << getpid() << std::endl;

//     while (true) // 多个进程同时退出 收到多个信号 有坑
//     {
//         // pid_t rid = waitpid(-1, nullptr, 0); // 回收、检测 一个没回收阻塞了
//         pid_t rid = waitpid(-1, nullptr, WNOHANG); // 非阻塞等待

//         if (rid > 0)
//         {
//             std::cout << "wait child sucess, rid: " << rid << std::endl;
//         }
//         else if (rid < 0)
//         {
//             std::cout << "wait child sucess" << std::endl;
//             break;
//         }
//         else
//         {
//             std::cout << "wait child sucess" << std::endl;
//             break;
//         }
//     }
// }

// void DoOtherThings()
// {
//     std::cout << "DoOtherThings" << std::endl;
// }

// int main()
// {
//     signal(SIGCHLD, notice);
//     for (int i = 0; i < 10; i++)
//     {
//         pid_t id = fork();
//         if (id == 0)
//         {
//             std::cout << "i am child, pid" << getpid() << " ppid: " << getppid() << std::endl;
//             sleep(3);
//             exit(1);
//         }
//     }

//     // father
//     while (true)
//     {
//         DoOtherThings();
//         sleep(1);
//     }
//     // sleep(100);
//     return 0;
// }

// volatile
// volatile int gflag = 0;
// void ChangeDate(int signo)
// {
//     std::cout << "get a signo, gflag change" << std::endl;
//     gflag = 1;
// }

// int main()
// {
//     signal(2, ChangeDate);
//     while(!gflag);
//     std::cout << "process quit!" << std::endl;
//     return 0;
// }

// sigaction 信号捕捉
//  void PrintPending(sigset_t &pending)
//  {
//      std::cout << "pid: " << getpid() << " curr process pending: ";
//      for (int signo = 31; signo >= 1; signo--)
//      {
//          if (sigismember(&pending, signo))
//          {
//              std::cout << 1;
//          }
//          else
//          {
//              std::cout << 0;
//          }
//      }
//      std::cout << std::endl;
//  }

// void handler(int signo)
// {
//     std::cout << "handler signo: " << signo << std::endl;
//     // sleep(100); // 正在处理2号信号时，会自动屏蔽其他发来的2号信号(处于pending状态)
//     while (true)
//     {
//         sigset_t pending;
//         sigpending(&pending);
//         PrintPending(pending);

//         sleep(1);
//         // sleep(3);
//         // break;
//     }
//     // exit(1);
// }

// int main()
// {
//     struct sigaction act, oact;
//     act.sa_handler = handler;
//     act.sa_flags = 0;
//     sigemptyset(&act.sa_mask); // 处理2号时，对其他信号也同时屏蔽
//     sigaddset(&act.sa_mask, 3);

//     // 对所有信号进行屏蔽
//     for (int i = 1; i <= 31; i++)
//     {
//         sigaction(i, &act, &oact);
//     }

//     //sigaction(2, &act, &oact);

//     while (true)
//     {
//         std::cout << "hello world! pid:" << getpid() << std::endl;
//         sleep(1);
//     }

//     return 0;
// }

// sigset_t
// 先阻塞2号信号-》获取pengding表-》发送2号-》获取pennding表
// 证明 阻塞产生信号永远处于未决状态
// void PrintPending(sigset_t &pending)
// {
//     std::cout << "pid: " << getpid() <<" curr process pending: ";
//     for (int signo = 31; signo >= 1; signo--)
//     {
//         if (sigismember(&pending, signo))
//         {
//             std::cout << 1;
//         }
//         else
//         {
//             std::cout << 0;
//         }
//     }
//     std::cout << std::endl;
// }

// void handler(int signo)
// {
//     std::cout << signo << " number has been deliveried" << std::endl;
// }

// int main()
// {
//     // 0. 捕捉2号
//     signal(2, handler);

//     // 1. 屏蔽2号信号
//     sigset_t block_set, old_set;
//     sigemptyset(&block_set);
//     sigemptyset(&old_set);
//     sigaddset(&block_set, SIGINT);

//     sigprocmask(SIG_BLOCK, &block_set, &old_set); // 修改当前进程的block表，完成对2号信号的屏蔽

//     int cnt = 20;
//     while (true)
//     {
//         // 2. 获取当前pengding表
//         sigset_t pending;
//         sigpending(&pending);
//         // 3. 打印pending表
//         PrintPending(pending);
//         cnt--;
//         // 4. 解除2号屏蔽
//         if (cnt == 0)
//         {
//             std::cout << "release block for number 2" << std::endl;
//             sigprocmask(SIG_SETMASK, &old_set, &block_set);
//         }

//         sleep(1);
//     }

//     return 0;
// }

// Core dump
// int Sum(int start, int end)
// {
//     int sum = 0;
//     for(int i = start; i <= end; i++)
//     {
//         sum /= 0;
//         sum += i;
//     }

//     return sum;
// }

// int main()
// {
//     pid_t id = fork();
//     if(0 == id)// child
//     {
//         sleep(1);
//         Sum(0,100);
//         exit(1);
//     }
//     //father
//     int status = 0;
//     pid_t rid = waitpid(id, &status, 0);
//     if(id == rid)
//     {
//         printf("exit code: %d, exit signal: %d, core dump: %d\n",
//                (status>>8)&0xFF, status&0x7F, (status>>7)&0x1);
//     }

//     //int total = Sum(0, 100);
//     //std::cout << "total: " << total << std::endl;

//     return 0;
// }

// 4. 异常
//  void handler(int sig)
//  {
//      std::cout << "handler sig:" << sig << std::endl;
//  }

// int main()
// {
//     //signal(8, handler);
//     //signal(11, handler);
//     int a = 10;
//     a /= 0;
//     while(true)
//     {
//         std::cout << "hello world!!! pid: " << getpid() << std::endl;

//         // int *p = nullptr;
//         // *p = 19l;
//         sleep(1);
//     }

//     return 0;
// }

// 3.闹钟alarm()
// int cnt = 1; // IO很慢

// void handler(int sig)
// {
//     std::cout << "handler sig:" << sig << " " << cnt << std::endl;
//     //exit(1);
// }

// int main()
// {
//     int n = alarm(1);
//     std::cout << n << std::endl;
//     //int cnt = 1;
//     signal(14, handler);
//     while(true)
//     {
//         //std::cout << "cnt: "<<cnt<<std::endl;
//         cnt++;
//         sleep(1);
//     }

//     return 0;
// }

// 2. 系统调用kill() abort()
// ./mykill 2 12343
// int main(int argc, char *argv[])
// {
//     if(argc != 3)
//     {
//         std::cerr << "Usage: " << argv[0] << " signum pid" << std::endl;
//     }
//     int signum = std::stoi(argv[1]);
//     pid_t pid = std::stoi(argv[2]);

//     kill(pid, signum);
//     return 0;
// }

// void handler(int sig)
// {
//     std::cout << "handler sig:" << sig << std::endl;
// }

// int main()
// {
//     int cnt = 0;
//     for (int i = 1; i <= 31; i++)
//     {
//         signal(i, handler);
//     }
//     while (true)
//     {
//         sleep(1);
//         std::cout << "hello world!!! pid: " << getpid() << std::endl;

//         // abort();
//     }

//     return 0;
// }

// 1.自定义捕捉信号
// void handler(int sig)
// {
//     std::cout << "handler sig:" << sig << std::endl;
// }

// int main()
// {
//     //信号捕捉只需捕捉一次
//     //signal(2, handler);
//     signal(3, handler);
//     //signal(4, handler);
//     //signal(5, handler);
//     while(true)
//     {
//         std::cout << "hello world!!! pid: " << getpid() << std::endl;
//         sleep(1);
//     }

//     return 0;
// }
