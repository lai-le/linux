#include <iostream>
#include <cerrno>  // errno.h
#include <cstring> // string.h
#include <unistd.h>
#include <sys/wait.h>
#include <string>
#include <sys/types.h>

const int size = 1024;

std::string getOtherMessage()
{
    static int cnt = 0;
    std::string messageid = std::to_string(cnt);
    cnt++;
    pid_t self_id = getpid();
    std::string stringpid = std::to_string(self_id);

    std::string message = " messageid: ";
    message += messageid;
    message += " my pid is: ";
    message += stringpid;

    return message;
}

// child --write
void SubProcessWrite(int wfd)
{
    int pipesize = 0;
    char c = 'A';
    std::string message = "father, i am your son process!";
    while (true)
    {
        // this info is sending to father
        std::string info = message + getOtherMessage();
        write(wfd, info.c_str(), info.size()); //'\0' be written? no！！！

        sleep(1); // need write slowly

        // pipesize
        //  write(wfd, &c, 1);
        //  std::cout << "pipesize: " << ++pipesize << "write charater is: " << c++ <<std::endl;
        //  if(c == 'G') break;
        //  sleep(1);
    }
    std::cout << "child quit...." << std::endl;
}

// father --read
void FatherProcessRead(int rfd)
{
    char inbuffer[size]; // c99, gnu g99 support
    while (true)
    {
        // sleep(500);
        ssize_t n = read(rfd, inbuffer, sizeof(inbuffer) - 1); //'\0' not read, reserve a place
        if (n > 0)
        {
            inbuffer[n] = 0; // n's place is '\0'
            std::cout << " father get message: " << inbuffer << std::endl;
        }
        else if (n == 0) // read the tail of message, return val is 0, child closed(pipefd[1])
        {
            std::cout << "child quit, father get val:" << n << " father quit too!" << std::endl;
            break;
        }
        else if (n < 0) // read error
        {
            std::cerr << "read error" << std::endl;
            break;
        }
        else
        {
        }

        sleep(1);
        break;

        std::cout << "father get return val: " << n << std::endl;
    }
}

int main()
{
    // 1. create pipe
    int pipefd[2];
    int n = pipe(pipefd); // Output type parameters, rfd, wfd
    if (n != 0)
    {
        std::cerr << "errno: " << errno << ": "
                  << "errstring: " << strerror(errno) << std::endl;
        return 1;
    }

    // pipefd[0] -> 0 -> r, pipefd[1] -> 1 -> w
    std::cout << "pipefd[0]: " << pipefd[0] << " pipefd[1]: " << pipefd[1] << std::endl;
    sleep(1);

    // 2. create child process
    pid_t id = fork();
    if (id == 0)
    {
        std::cout << "child has closed unnecessary fd, is readying to send message" << std::endl;
        sleep(1);
        // child --write
        // 3. close unnecessary fd
        close(pipefd[0]);
        SubProcessWrite(pipefd[1]);
        close(pipefd[1]);
        exit(0);
    }

    // father --read
    // 3. close unnecessary fd
    std::cout << "father has closed unnecessary fd, is readying to receive message" << std::endl;
    close(pipefd[1]);
    FatherProcessRead(pipefd[0]);
    std::cout << "after 5s, father close rfd" << std::endl;
    close(pipefd[0]);

    int status = 0;
    pid_t rid = waitpid(id, &status, 0);
    if (rid > 0)
    {
        std::cout << "wait child process done, exit sig: " << (status & 0x7f) << std::endl;
        std::cout << "wait child process done, exit code(ign): " << ((status >> 8) & 0xFF) << std::endl;
    }

    return 0;
}