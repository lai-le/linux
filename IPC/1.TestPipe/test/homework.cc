#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <cstdio>
#include <string>
#include <sys/types.h>
#include <sys/wait.h>

int main()
{
    int pipefd[2];
    int res = pipe(pipefd);
    if (res < 0)
    {
        perror("pipe");
        return -1;
    }

    pid_t id = fork();
    if (id == 0)
    {
        // child
        close(pipefd[1]);
        // read
        char buffer[1024];
        while (true)
        {
            int n = read(pipefd[0], buffer, sizeof(buffer));
            if (n > 0)
            {
                buffer[n] = 0;
                std::cout << buffer << std::endl;
            }
            else
            {
                break;
            }
        }
        close(pipefd[0]);
        exit(0);
    }

    // father write
    close(pipefd[0]);
    std::string message = "I am father";
    write(pipefd[1], message.c_str(), message.size());
    close(pipefd[1]);

    pid_t n = waitpid(id, nullptr, 0);
    if(n > 0)
    {
        std::cout << "wait success" << std::endl;
    }

    return 0;
}