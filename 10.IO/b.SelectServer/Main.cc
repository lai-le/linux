#include <iostream>
#include <memory>

#include "SelectServer.hpp"

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cerr << "udpserve argv Usage: " << argv[0] << " serverport" << std::endl;
        exit(-1);
    }

    uint16_t port = std::stoi(argv[1]);

    std::unique_ptr<SelectServer> ssvr = std::make_unique<SelectServer>(port);
    ssvr->InitServer();
    ssvr->Loop();

    return 0;
}
