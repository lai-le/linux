#pragma once

#include <iostream>
#include <unistd.h>
#include <sys/select.h>
#include "Log.hpp"
#include "InetAddr.hpp"
#include "Socket.hpp"

using namespace socket_ns;
using namespace log_ns;

const static int gnum = sizeof(fd_set) * 8; // 1024
const static int defaultarray = -1;

class SelectServer
{
public:
    SelectServer(uint16_t port)
        : _listensock(std::make_unique<TcpSocket>()),
          _port(port)
    {
        _listensock->BuildListenSocket(port);
    }
    void InitServer()
    {
        for (int i = 0; i < gnum; i++)
        {
            fd_array[i] = defaultarray;
        }
        fd_array[0] = _listensock->GetSockfd(); // 默认添加 listensockfd
    }

    void HandleAccepter()
    {
        InetAddr addr;
        int sockfd = _listensock->Accepter(&addr);
        if (sockfd > 0)
        {
            LOG(INFO, "get a new link, client: %s\n", addr.AddrStr().c_str());
        }
        else
        {
            LOG(ERROR, "accept fail!\n");
            return;
        }

        // accept->sockfd, 不能直接读 条件不一定满足 所以要把它添加到 select 托管
        // 创建一个数组 保存 合法的fd
        bool flag = false; // 可能 fd_array 满了
        for (int i = 0; i < gnum; i++)
        {
            if (fd_array[i] == defaultarray)
            {
                flag = true;
                fd_array[i] = sockfd;
                LOG(DEBUG, "add new sockfd: %d to fd_array\n", sockfd);
                break;
            }
        }
        // 满了 关掉新连接
        if (flag == false)
        {
            LOG(WARNING, "fd_arrat is full!\n");
            ::close(sockfd);
        }
    }

    void HandleIO(int i)
    {
        char buffer[1024];
        ssize_t n = ::recv(fd_array[i], buffer, sizeof(buffer) - 1, 0); // 不会阻塞
        if (n > 0)
        {
            buffer[n] = 0;
            std::cout << "client say# " << buffer << std::endl;
            std::string content = "<html><body><h1>Hello bit! Hello select!</html></body></h1>";
            std::string echo_str = "HTTP/1.1 200 OK\r\n\r\n";
            echo_str += content;

            ::send(fd_array[i], echo_str.c_str(), echo_str.size(), 0);
        }
        else if (n == 0)
        {
            LOG(INFO, "client quit...\n");
            ::close(fd_array[i]);
            fd_array[i] = defaultarray; // 不要再关心这个fd了
        }
        else
        {
            LOG(ERROR, "recv error...\n");
            ::close(fd_array[i]);
            fd_array[i] = defaultarray; // 不要再关心这个fd了
        }
    }

    void HandleEvents(fd_set &rfds)
    {
        // 一定会同时存在大量的fd就绪 可能是listensockfd 可能是普通sockfd
        for (int i = 0; i < gnum; i++)
        {
            if (fd_array[i] != defaultarray && FD_ISSET(fd_array[i], &rfds)) // 过滤
            {
                // 读事件就绪 1. listensockfd 2.普通sockfd
                if (_listensock->GetSockfd() == fd_array[i])
                {
                    // listensockfd
                    // 存在说明 已经就绪了
                    HandleAccepter();
                }
                else
                {
                    // 普通sockfd 正常读写
                    HandleIO(i);
                }
            }
        }
    }

    void Loop()
    {
        InetAddr addr;
        while (true)
        {
            // _listensock->Accepter(&addr); // 不能直接调用 会阻塞

            // 1. init rfds
            fd_set rfds;
            struct timeval timeout = {5, 0};
            FD_ZERO(&rfds);
            // FD_SET(_listensock->GetSockfd(), &rfds);

            // 2. add legal fd to rfds && update a biggest fd
            int biggestfd = defaultarray;
            for (int i = 0; i < gnum; i++)
            {
                if (fd_array[i] == defaultarray)
                    continue;

                FD_SET(fd_array[i], &rfds);
                if (biggestfd < fd_array[i])
                    biggestfd = fd_array[i];
            }

            int n = ::select(biggestfd + 1, &rfds, nullptr, nullptr, nullptr);
            if (n > 0)
            {
                sleep(1);
                // success, return ready fd numbers
                LOG(INFO, "select success! Some fds have been ready! the count is n: %d\n", n);

                // handle events, Distribute events
                HandleEvents(rfds);
                PrintDebug();
                // continue;
            }
            else if (n == 0)
            {
                // timeout
                LOG(WARNING, "wait timeout: %d.%d\n", timeout.tv_sec, timeout.tv_usec);
                // continue;
            }
            else
            {
                // select error
                LOG(ERROR, "select error! n: %d\n", n);
                // return;
            }
        }
    }

    void PrintDebug()
    {
        std::cout << "fd list: ";
        for (int i = 0; i < gnum; i++)
        {
            if (fd_array[i] != defaultarray)
                std::cout << fd_array[i] << " ";
        }
        std::cout << "\n";
    }

    ~SelectServer() {}

private:
    uint16_t _port;
    std::unique_ptr<TcpSocket> _listensock;

    int fd_array[gnum]; // 保存合法的 fd
};