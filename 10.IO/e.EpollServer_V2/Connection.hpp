#pragma once
#include <iostream>
#include <string>
#include <functional>
#include "Socket.hpp"
#include "Reactor.hpp"
#include "InetAddr.hpp"

/*
*connection类
*对 sockfd 和缓冲区 以及 接受、发送、异常方法 的管理
*/
#define ListenConnection 0
#define NormalConnection 1

class Connection;
class Reactor;
using handle_t = std::function<void(Connection *)>;

class Connection
{
public:
    Connection(int fd)
        : _sockfd(fd),
          _R(nullptr)
    {
    }

    void RegisterHandler(handle_t recver, handle_t sender, handle_t excepter)
    {
        _handle_recver = recver;
        _handle_sender = sender;
        _handle_excepter = excepter;
    }

    void SetEvents(uint32_t events) // 设置到connections
    {
        _events = events;
    }

    int GetSockfd()
    {
        return _sockfd;
    }

    uint32_t GetEvents()
    {
        return _events;
    }

    void SetReactor(Reactor *reactor)
    {
        _R = reactor;
    }

    void SetConnection(int model)
    {
        _type = model;
    }

    void SetInetAddr(const InetAddr &addr)
    {
        _addr = addr;
    }

    int TypeOfConn()
    {
        return _type;
    }

    // 读到的添加到 读缓冲区
    void AppendInBuffer(const std::string &in)
    {
        _inbuffer += in;
    }

    std::string& GetInbuffer()
    {
        return _inbuffer;
    }

    // 丢弃掉 已经发送了的内容
    void DiscardOutBuffer(int n)
    {
        _outbuffer.erase(0, n);
    }

    // 添加 处理后的应答 统一发回
    void AppendOutBuffer(const std::string &in)
    {
        _outbuffer += in;
    }

    std::string& GetOutBuffer()
    {
        return _outbuffer;
    }

    // 关闭
    void CloseSockfd() 
    {
        if(_sockfd > 0)
        {
            ::close(_sockfd);
        }
    }

    ~Connection() {}

private:
    int _sockfd;
    uint32_t _events;
    std::string _inbuffer;  // 输入缓冲区
    std::string _outbuffer; // 输出缓冲区
    int _type;              // 设置 监听 或者 普通fd模式
    InetAddr _addr;

public:
    // IO处理 回调
    handle_t _handle_recver;
    handle_t _handle_sender;
    handle_t _handle_excepter;

    Reactor *_R; // 回指向Reactor
};