#pragma once
// #include <memory>
#include <sys/types.h>
#include <sys/socket.h>
#include "Connection.hpp"
#include "Log.hpp"

/*
*HandleConnection
*专门用来处理普通sockfd的模块
*具有HandleRecver、HandleSender、HandleExcepter等方法
*/
using namespace log_ns;

const static int buffersize = 1024;

class HandleConnection
{
public:
    HandleConnection(handle_t parse) : _parse(parse)
    {
    }

    // 接受数据到缓冲区 并解析
    void HandleRecver(Connection *conn)
    {
        errno = 0;
        // LOG(DEBUG, "HandleRecver sockfd is: %d\n", conn->GetSockfd());
        while (true)
        {
            char buffer[buffersize];
            ssize_t n = ::recv(conn->GetSockfd(), buffer, sizeof(buffer) - 1, 0);
            if (n > 0) // 读成功
            {
                // buffer[n - 2] = 0;
                buffer[n] = 0;
                // 不处理 添加到conntion的读缓冲区
                conn->AppendInBuffer(buffer);
            }
            else
            {
                if (errno == EWOULDBLOCK) // 读条件不满足
                {
                    break;
                }
                else if (errno == EAGAIN) // 信号中断
                {
                    continue;
                }
                else // 读失败
                {
                    LOG(ERROR, "HandleRecver recv fail!\n");
                    conn->_handle_excepter(conn); // 统一处理异常
                    return;
                }
            }
        }

        std::cout << conn->GetInbuffer() << std::endl;
        // 数据包解析
        _parse(conn);
    }

    // 接受应答到缓冲区 统一发送
    void HandleSender(Connection *conn)
    {
        errno = 0;
        while (true)
        {
            ssize_t n = ::send(conn->GetSockfd(), conn->GetOutBuffer().c_str(), conn->GetOutBuffer().size(), 0);
            if (n > 0)
            {
                // 发送完丢弃
                conn->DiscardOutBuffer(n);
                if (conn->GetOutBuffer().empty())
                    break;
            }
            else if (n == 0)
            {
                break;
            }
            else
            {
                if (errno == EWOULDBLOCK) // 发送条件不满足
                {
                    break;
                }
                else if (errno == EAGAIN)
                {
                    continue;
                }
                else // 发送失败 统一异常处理
                {
                    LOG(ERROR, "send fail!\n");
                    conn->_handle_excepter(conn);
                    return;
                }
            }
        }

        // 发送条件不满足 || 缓冲区有数据
        // 托管给 epoll自动发送
        if (!conn->GetOutBuffer().empty())
        {
            conn->_R->EnableConnReadOrWrite(conn->GetSockfd(), true, true);
        }
        else
        {
            conn->_R->EnableConnReadOrWrite(conn->GetSockfd(), true, false);
        }
    }

    // 整个代码所有的逻辑异常
    void HandleExcepter(Connection *conn)
    {
        // 删除链接
        conn->_R->DelConnction(conn->GetSockfd());
    }
    ~HandleConnection() {}

private:
    handle_t _parse; // 数据包解析调用
};