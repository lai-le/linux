#pragma once

#include <iostream>
#include <functional>
#include <memory>
#include <sys/types.h>
#include <sys/socket.h>

#include "InetAddr.hpp"
#include "Socket.hpp"
#include "Protocol.hpp"
#include "NetCal.hpp"

#include "Connection.hpp"

/*
Service类 
*发过来的报文进行解析、处理、发回、序列化、反序列化等操作
*/

using namespace socket_ns;

// using work_t = std::function<std::shared_ptr<Response>(std::shared_ptr<Request>)>;

class Service
{
public:
    Service()
    {
    }
    // Service(work_t work) : _work(work)
    // {
    // }

    void ParseIO(Connection *conn)
    {
        while (true)
        {
            // 1. 接受请求
            // std::string message = conn->GetInbuffer();
            // 2. 提取完整报文
            std::string package = DeCode(conn->GetInbuffer());
            if (package.empty())
                break;
            std::cout << "package: \n"
                      << package << std::endl;

            // 3. 反序列化
            auto req = Factory::BuildRequestObj();
            if (req->Deserialize(package) == false)
            {
                std::cerr << "service deserialize fail!" << std::endl;
            }

            // 4. 业务处理
            auto resp = _cal.Calculate(req);
            // std::shared_ptr<Response> resp = _work(req);

            // 5. 序列化
            std::string r_layload;
            resp->serialize(&r_layload);
            std::cout << "respjson: \n"
                      << r_layload << std::endl;

            // 6. 添加报头
            r_layload = EnCode(r_layload);
            std::cout << "respjson add header done: \n"
                      << r_layload << std::endl;

            // 7. 发回  添加到发送缓冲区 统一发送
            conn->AppendOutBuffer(r_layload);
        }

        // 至少处理完一个请求
        // 方法一：直接发送
        //  if (!conn->GetOutBuffer().empty())
        //     conn->_handle_sender(conn);
        // 方法二：只需要激活对该fd的关心即可
        if (!conn->GetOutBuffer().empty())
            conn->_R->EnableConnReadOrWrite(conn->GetSockfd(), true, true);
    }

    ~Service()
    {
    }

private:
    // work_t _work;
    Calculator _cal;
};