#pragma once

#include <unistd.h>
#include <fcntl.h>
#include <iostream>

/*
* 公共代码 全局函数
* 描述: 枚举的错误码、设定非阻塞
*/
enum
{
    SOCKET_ERROR = 1,
    BIND_ERROR,
    LISTEN_ERROR,
    EPOLL_CREATE_ERROR
};

/*
* 函数名: SetNonBlock
* 描述: 设定sockfd为非阻塞状态
* 参数: 
* - num1: fd
* 返回值: 无
*/
void SetNonBlock(int fd)
{
    int f1 = fcntl(fd, F_GETFL);
    if(f1 < 0)
    {
        std::cerr << "fcntl fail!" << std::endl;
        return;
    }
    fcntl(fd, F_SETFL, f1 | O_NONBLOCK);
}
