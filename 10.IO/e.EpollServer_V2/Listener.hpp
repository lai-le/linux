#pragma once
#include <memory>

#include "Socket.hpp"
#include "Connection.hpp"
#include "InetAddr.hpp"

/*
*Listener类
*专门用来获取链接的模块
*/
using namespace socket_ns;

class Listener
{
public:
    Listener(uint16_t port)
        : _port(port),
          _listensock(std::make_unique<TcpSocket>())
    {
        _listensock->BuildListenSocket(_port);
    }

    void Accepter(Connection *conn)
    {
        // 设置fd为非阻塞
        while (true)
        {
            InetAddr addr;
            int code;
            int sockfd = _listensock->Accepter(&addr, &code);
            if (sockfd > 0)
            {
                LOG(INFO, "get a new connection, client info: %s, port: %d, sockfd: %d\n", addr.AddrStr().c_str(), _port, sockfd);
                // 将sockfd托管
                // #define NormalConnection 1  #define ListenConnection 0
                conn->_R->AddConnection(sockfd, EPOLLIN | EPOLLET, addr, NormalConnection);
            }
            else
            {
                if (errno == EWOULDBLOCK)
                {
                    // LOG(INFO, "底层链接全部获取完毕\n");
                    LOG(INFO, "all connections have been obtained\n");
                    return;
                }
                else if (errno == EAGAIN)
                {
                    continue;
                }
                else
                {
                    LOG(ERROR, "获取链接失败\n");
                    break;
                }
            }
        }
    }

    int Listensockfd()
    {
        return _listensock->Sockfd();
    }

    ~Listener() {}

public:
    uint16_t _port;
    std::unique_ptr<Socket> _listensock;
};