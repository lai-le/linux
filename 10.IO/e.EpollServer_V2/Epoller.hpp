#pragma once
#include <iostream>
#include <sys/epoll.h>

#include "Log.hpp"
#include "Common.hpp"

/*
*Multiplex类 作为模板方法类
*Epoller类 对epoll的相关操作进行封装
*专门用来处理普通sockfd的模块
*添加、修改、删除、获取就绪事件fd及其事件关心
*/

using namespace log_ns;

const static int gsize = 128;

class Multiplex
{
public:
    virtual bool AddEvents(int fd, uint32_t events) = 0;
    virtual int EpollWait(struct epoll_event revs[], int num, int timeout) = 0;
    virtual bool ModEvents(int fd, uint32_t events) = 0;
    virtual void DelEvents(int fd) = 0;
};

class Epoller : public Multiplex
{
private:
    bool ModEventsHelper(int fd, uint32_t events, int oper)
    {
        struct epoll_event ev;
        ev.data.fd = fd;
        ev.events = events;

        int n = epoll_ctl(_epfd, oper, fd, &ev);
        if (n < 0)
        {
            LOG(ERROR, "epoll_ctl add %d and events %s are failed\n", fd, EventsToString(events).c_str());
            return false;
        }
        LOG(INFO, "epoll_ctl add fd: %d and events %s are success\n", fd, EventsToString(events).c_str());
        return true;
    }

public:
    Epoller()
    {
        _epfd = ::epoll_create(gsize);
        if (_epfd < 0)
        {
            LOG(FATAL, "epoll create fail!\n");
            exit(EPOLL_CREATE_ERROR);
        }
        // SetNonBlock(_epfd);
        LOG(INFO, "epoll create success! epfd: %d\n", _epfd);
    }

    std::string EventsToString(uint32_t events)
    {
        std::string eventstr;
        if (events & EPOLLIN)
            eventstr = "EPOLLIN";
        if (events & EPOLLOUT)
            eventstr += "|EPOLLOUT";
        if (events & EPOLLET)
            eventstr += "|EPOLLET";
        return eventstr;
    }

    bool AddEvents(int fd, uint32_t events) override
    {
        if (!ModEventsHelper(fd, events, EPOLL_CTL_ADD))
            return false;
        else
            return true;
    }

    bool ModEvents(int fd, uint32_t events) override // 修改 fd托管的事件
    {
        if (ModEventsHelper(fd, events, EPOLL_CTL_MOD))
            return false;
        else
            return true;

        // struct epoll_event ev;
        // ev.data.fd = fd;
        // ev.events = events;

        // epoll_ctl(_epfd, EPOLL_CTL_MOD, fd, &ev);
    }

    void DelEvents(int fd)
    {
        ::epoll_ctl(_epfd, EPOLL_CTL_DEL, fd, nullptr);
    }

    int EpollWait(struct epoll_event revs[], int num, int timeout) override
    {
        int n = ::epoll_wait(_epfd, revs, num, timeout);
        if (n > 0)
        {
            // epoll_wait success
            LOG(INFO, "epoll_wait success! %d fds have been ready!\n", n);
        }
        else if (n == 0)
        {
            // timeout
            LOG(WARNING, "epoll_wait timeout\n");
        }
        else
        {
            // epoll_wait fail
            LOG(ERROR, "epoll_wait fail!\n");
        }

        return n;
    }

    ~Epoller()
    {
    }

private:
    int _epfd;
};