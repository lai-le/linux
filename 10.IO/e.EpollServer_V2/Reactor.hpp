#pragma once
#include <unordered_map>
#include <memory>
#include <string>
#include <iostream>

#include "Connection.hpp"
#include "Epoller.hpp"
#include "InetAddr.hpp"

/*
*Reactor类
*作为服务器的主逻辑
*对一个个的链接进行管理
*Reactor 中添加处理 socket的方法集
*具有：添加链接AddConnection、分发任务Dispatcher、
*删除链接DelConnction、控制 托管给epoll的事件状态EnableConnReadOrWrite等功能
*/

const static int gnum = 64;
class Reactor
{
public:
    void SetOnConnect(handle_t onconnect)
    {
        _OnConnect = onconnect;
    }
    void SetNormalHandle(handle_t onreceiver, handle_t onsender, handle_t onexcepter)
    {
        _OnReceiver = onreceiver;
        _OnSender = onsender;
        _OnExceptor = onexcepter;
    }

public:
    Reactor() : _epoller(std::make_unique<Epoller>()), _isrunning(false)
    {
    }

    // void AddConnection(int fd, uint32_t events, handle_t recver, handle_t sender, handle_t excepter)
    void AddConnection(int fd, uint32_t events, const InetAddr &addr, int type)
    {
        // 1. construct a connection object
        Connection *conn = new Connection(fd);
        conn->SetEvents(events);
        conn->SetConnection(type);
        conn->SetInetAddr(addr);
        conn->SetReactor(this);
        // conn->RegisterHandler(recver, sender, excepter);
        // 根据 设置的connection类型来判断 要执行什么方法
        // #define ListenConnection 0
        // #define NormalConnection 1
        if (conn->TypeOfConn() == ListenConnection)
        {
            // 监听方法执行
            conn->RegisterHandler(_OnConnect, nullptr, nullptr);
        }
        else
        {
            // 普通读写方法执行
            conn->RegisterHandler(_OnReceiver, _OnSender, _OnExceptor);
        }

        // 2. fd 和 events 写到kernel 托管给epoll
        if (!_epoller->AddEvents(conn->GetSockfd(), conn->GetEvents()))
        {
            return;
        }

        // 3. 托管给 _connection
        _connections.insert(std::make_pair(fd, conn));
    }

    // 控制 托管给epoll的事件状态
    void EnableConnReadOrWrite(int sockfd, bool readable, bool writeable)
    {
        if(!IsExistConnection(sockfd))  return;

        uint32_t events = ((readable ? EPOLLIN : 0) | (writeable ? EPOLLOUT : 0) | EPOLLET);
        // 写到connections
        _connections[sockfd]->SetEvents(events);
        // 写到 内核 epoll
        _epoller->ModEvents(_connections[sockfd]->GetSockfd(), _connections[sockfd]->GetEvents());
    }

    void LoopOnce(int timeout)
    {
        int n = _epoller->EpollWait(revs, gnum, timeout);
        if (n > 0)
        {
            for (int i = 0; i < n; i++)
            {
                int sockfd = revs[i].data.fd;
                uint32_t revents = revs[i].events;

                if ((revents & EPOLLERR) || (revents & EPOLLHUP)) // 出错了 设置为就绪 在读写的时候统一处理异常
                {
                    revents |= (EPOLLOUT | EPOLLIN);
                }
                else if ((revents & EPOLLIN)) // 读事件
                {
                    if (IsExistConnection(sockfd) && _connections[sockfd]->_handle_recver)
                    {
                        _connections[sockfd]->_handle_recver(_connections[sockfd]);
                    }
                }
                if (revents & EPOLLOUT) // 写事件
                {
                    if (IsExistConnection(sockfd) && _connections[sockfd]->_handle_sender)
                    {
                        _connections[sockfd]->_handle_sender(_connections[sockfd]);
                    }
                }
            }
        }
    }

    // 事件派发
    void Dispatcher()
    {
        _isrunning = true;
        int timeout = -1;
        while (_isrunning)
        {
            LoopOnce(timeout);
            // do otherthing
            PrintDebug();
        }
        _isrunning = false;
    }

    void DelConnction(int sockfd)
    {
        if(!IsExistConnection(sockfd)) return;

        LOG(INFO, "Delete sockfd: %d\n", sockfd);
        // 1. 在内核中移除对 sockfd 的关心
        EnableConnReadOrWrite(sockfd, false, false);
        _epoller->DelEvents(sockfd);
        // 2. sockfd 关闭
        _connections[sockfd]->CloseSockfd();
        // 3. 在_connections中移除对sockfd的关心
        delete _connections[sockfd];
        _connections.erase(sockfd);
    }

    bool IsExistConnection(int sockfd)
    {
        return _connections.find(sockfd) != _connections.end();
    }

    void PrintDebug()
    {
        std::string fdlist;
        for(auto &conn : _connections)
        {
            fdlist += std::to_string(conn.second->GetSockfd()) + " ";
        }
        LOG(DEBUG, "connection fd list: %s\n", fdlist.c_str());
    }

    ~Reactor() {}

private:
    std::unordered_map<int, Connection *> _connections; // int->fd
    std::unique_ptr<Multiplex> _epoller;

    bool _isrunning;
    struct epoll_event revs[gnum]; // 系统交给用户

    // Reactor 中添加处理 socket的方法集
    // 1. 处理监听连接
    handle_t _OnConnect;
    // 2. 处理普通链接
    handle_t _OnReceiver;
    handle_t _OnSender;
    handle_t _OnExceptor;
};