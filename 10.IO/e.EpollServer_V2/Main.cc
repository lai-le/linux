#include <iostream>

#include "Reactor.hpp"
#include "Connection.hpp"
#include "Listener.hpp"
#include "HandleConnection.hpp"
#include "Service.hpp"

/*
*主逻辑
*绑定好 处理链接的方法 和 普通sockfd的IO方法
*添加监听fd 并开始进行事件派发
*/

//上层 把具体的方法绑定 reactor获取到就绪事件后 分发任务
//经过connection注册好任务 
//当客户端发送请求 通过协议 和 相关处理 构建应答 

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cerr << "Usage: " << argv[0] << " local-port" << std::endl;
        exit(0);
    }
    uint16_t port = std::stoi(argv[1]);
    InetAddr localaddr("0.0.0.0", port);

    Service serv; // 业务对象

    // 专门用来处理新连接的模块
    Listener listener(port);
    // 专门用来处理普通sockfd的模块
    HandleConnection handles(std::bind(&Service::ParseIO, &serv, std::placeholders::_1));

    std::unique_ptr<Reactor> R = std::make_unique<Reactor>();
    R->SetOnConnect(std::bind(&Listener::Accepter, &listener, std::placeholders::_1));
    R->SetNormalHandle(std::bind(&HandleConnection::HandleRecver, &handles, std::placeholders::_1),
                       std::bind(&HandleConnection::HandleSender, &handles, std::placeholders::_1),
                       std::bind(&HandleConnection::HandleExcepter, &handles, std::placeholders::_1));

    R->AddConnection(listener.Listensockfd(), EPOLLIN | EPOLLET, localaddr, ListenConnection);

    // 事件派发
    R->Dispatcher();

    return 0;
}