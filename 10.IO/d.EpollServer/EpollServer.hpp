#pragma once

#include <iostream>
#include <unistd.h>
// #include <sys/select.h>
// #include <poll.h>
#include <sys/epoll.h>
#include "Log.hpp"
#include "InetAddr.hpp"
#include "Socket.hpp"

using namespace socket_ns;
using namespace log_ns;

const static int gnum = sizeof(fd_set) * 8; // 1024
const static int gsize = 100;

class EpollServer
{
public:
    EpollServer(uint16_t port)
        : _listensock(std::make_unique<TcpSocket>()),
          _port(port)
    {
        _listensock->BuildListenSocket(port);

        _epfd = ::epoll_create(gsize);
        if (_epfd < 0)
        {
            LOG(FATAL, "epoll_create fail!\n");
        }
        LOG(INFO, "epoll_create sucess!, epfd is: %d\n", _epfd);
    }
    void InitServer()
    {
        struct epoll_event events;
        events.data.fd = _listensock->GetSockfd();
        events.events = EPOLLIN;
        int n = ::epoll_ctl(_epfd, EPOLL_CTL_ADD, _listensock->GetSockfd(), &events);
        if (n < 0)
        {
            LOG(ERROR, "fail to add listensock!\n");
            exit(-1);
        }
        LOG(INFO, "epoll_ctl success, add new sockfd : %d\n", _listensock->GetSockfd());
    }

    std::string EventsToString(uint32_t events)
    {
        std::string eventstr;
        if (events & EPOLLIN)
            eventstr = "EPOLLIN";
        if (events & EPOLLOUT)
            eventstr += "|EPOLLOUT";
        return eventstr;
    }

    void HandleAccepter()
    {
        InetAddr addr;
        int sockfd = _listensock->Accepter(&addr);
        if (sockfd > 0)
        {
            LOG(INFO, "get a new link, client: %s\n", addr.AddrStr().c_str());
        }
        else
        {
            LOG(ERROR, "accept fail!\n");
            return;
        }

        struct epoll_event events;
        events.data.fd = sockfd;
        events.events = EPOLLIN;
        int n = ::epoll_ctl(_epfd, EPOLL_CTL_ADD, sockfd, &events);
        if (n == 0)
        {
            LOG(INFO, "add listensock to epoll! sockfd: %d\n", sockfd);
        }
    }

    void HandleIO(int fd)
    {
        char buffer[4096];
        ssize_t n = ::recv(fd, buffer, sizeof(buffer) - 1, 0); // 不会阻塞
        if (n > 0)
        {
            buffer[n] = 0;
            std::cout << "client say# " << buffer << std::endl;
            std::string content = "<html><body><h1>Hello bit! Hello select!</html></body></h1>";
            std::string echo_str = "HTTP/1.1 200 OK\r\n\r\n";
            echo_str += content;

            ::send(fd, echo_str.c_str(), echo_str.size(), 0);
        }
        else if (n == 0)
        {
            LOG(INFO, "client quit...\n");
            // 移除时必须保证这个fd合法 否则会出错
            int n = epoll_ctl(_epfd, EPOLL_CTL_DEL, fd, nullptr);
            ::close(fd);
        }
        else
        {
            LOG(ERROR, "recv error...\n");
            int n = epoll_ctl(_epfd, EPOLL_CTL_DEL, fd, nullptr);
            ::close(fd);
        }
    }

    void HandleEvents(int n)
    {
        // 一定会同时存在大量的fd就绪 可能是listensockfd 可能是普通sockfd
        for (int i = 0; i < n; i++)
        {
            int fd = _revents[i].data.fd;
            uint32_t revents = _revents[i].events;
            if (revents & EPOLLIN)
            {
                LOG(INFO, "some fds have been ready, fd: %d, events: %s\n", fd, EventsToString(revents).c_str());
                if (fd == _listensock->GetSockfd()) // 过滤
                {
                    HandleAccepter();
                }
                else
                {
                    // 普通sockfd 正常读写
                    HandleIO(fd);
                }
            }
        }
    }

    void Loop()
    {
        int timeout = -1;
        while (true)
        {
            int n = ::epoll_wait(_epfd, _revents, gnum, timeout);
            if (n > 0)
            {
                sleep(1);
                // LOG(INFO, "select success! Some fds have been ready! the count is n: %d\n", n);

                HandleEvents(n);
                // PrintDebug();
            }
            else if (n == 0)
            {

                LOG(WARNING, "wait timeout\n");
            }
            else
            {
                LOG(ERROR, "select error! n: %d\n", n);
            }
        }
    }

    // void PrintDebug()
    // {
    //     std::cout << "fd list: ";
    //     for (int i = 0; i < gnum; i++)
    //     {
    //         if (_revents[i].data.fd != defaultarray)
    //             std::cout << _revents[i].data.fd << " ";
    //     }
    //     std::cout << "\n";
    // }

    ~EpollServer() 
    {
        if(_epfd > 0)
        {
            ::close(_epfd);
        }
        _listensock->CloseSockfd();
    }

private:
    uint16_t _port;
    std::unique_ptr<TcpSocket> _listensock;
    int _epfd;

    // struct pollfd fd_events[gnum]; // 保存合法的 fd
    struct epoll_event _revents[gnum];
};