#include <iostream>
#include <unistd.h>

#include "Comm.hpp"

int main()
{
    char buffer[1024] = {0};
    SetNonBlock(0);
    while (true)
    {
        // sleep(1);
        // printf("Enter# ");
        // fflush(stdout);

        int n = ::read(0, buffer, sizeof(buffer) - 1);
        if (n > 0)
        {
            buffer[n] = 0;
            // printf("echo# %s", buffer);
            std::cout << "echo# " << buffer;
        }
        else if (n == 0)
        {
            printf("read done!\n");
            break;
        }
        else
        {
            if (errno == EWOULDBLOCK || errno == EAGAIN)
            {
                sleep(1);
                std::cout << "no data begin, is looping..." << std::endl;
                std::cout << "you can do other things..." << std::endl;
                // continue;
            }
            else if (errno == EINTR)
            {
                std::cout << "signal interrupt..." << std::endl;
                continue;
            }
            else
            {
                perror("read\n");
                break;
            }

            // perror("read\n");
            // printf("n=%d\n", n);
            // break;
        }
    }

    return 0;
}