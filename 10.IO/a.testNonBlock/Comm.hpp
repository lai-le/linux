#pragma once

#include <unistd.h>
#include <fcntl.h>
#include <iostream>

void SetNonBlock(int fd)
{
    int f1 = fcntl(fd, F_GETFL);
    if(f1 < 0)
    {
        std::cerr << "fcntl fail!" << std::endl;
        return;
    }
    fcntl(fd, F_SETFL, f1 | O_NONBLOCK);
}