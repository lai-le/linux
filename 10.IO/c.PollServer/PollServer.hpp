#pragma once

#include <iostream>
#include <unistd.h>
// #include <sys/select.h>
#include <poll.h>
#include "Log.hpp"
#include "InetAddr.hpp"
#include "Socket.hpp"

using namespace socket_ns;
using namespace log_ns;

const static int gnum = sizeof(fd_set) * 8; // 1024
const static int defaultarray = -1;

class PollServer
{
public:
    PollServer(uint16_t port)
        : _listensock(std::make_unique<TcpSocket>()),
          _port(port)
    {
        _listensock->BuildListenSocket(port);
    }
    void InitServer()
    {
        for (int i = 0; i < gnum; i++)
        {
            fd_events[i].fd = defaultarray;
            fd_events[i].events = 0;
            fd_events[i].revents = 0;
        }
        fd_events[0].fd = _listensock->GetSockfd(); // 默认添加 listensockfd
        fd_events[0].events = POLLIN;
    }

    void HandleAccepter()
    {
        InetAddr addr;
        int sockfd = _listensock->Accepter(&addr);
        if (sockfd > 0)
        {
            LOG(INFO, "get a new link, client: %s\n", addr.AddrStr().c_str());
        }
        else
        {
            LOG(ERROR, "accept fail!\n");
            return;
        }

        bool flag = false; // 可能 fd_array 满了
        for (int i = 0; i < gnum; i++)
        {
            if (fd_events[i].fd == defaultarray)
            {
                flag = true;
                fd_events[i].fd = sockfd;
                fd_events[i].events = POLLIN;
                LOG(DEBUG, "add new sockfd: %d to fd_array\n", sockfd);
                break;
            }
        }
        if (flag == false)
        {
            LOG(WARNING, "fd_arrat is full!\n");
            ::close(sockfd);
            // 扩容
        }
    }

    void HandleIO(int i)
    {
        char buffer[1024];
        ssize_t n = ::recv(fd_events[i].fd, buffer, sizeof(buffer) - 1, 0); // 不会阻塞
        if (n > 0)
        {
            buffer[n] = 0;
            std::cout << "client say# " << buffer << std::endl;
            std::string content = "<html><body><h1>Hello bit! Hello select!</html></body></h1>";
            std::string echo_str = "HTTP/1.1 200 OK\r\n\r\n";
            echo_str += content;

            ::send(fd_events[i].fd, echo_str.c_str(), echo_str.size(), 0);
        }
        else if (n == 0)
        {
            LOG(INFO, "client quit...\n");
            ::close(fd_events[i].fd);
            fd_events[i].fd = defaultarray; // 不要再关心这个fd了
            fd_events[i].events = 0;
            fd_events[i].revents = 0;
        }
        else
        {
            LOG(ERROR, "recv error...\n");
            ::close(fd_events[i].fd);
            fd_events[i].fd = defaultarray; // 不要再关心这个fd了
            fd_events[i].events = 0;
            fd_events[i].revents = 0;
        }
    }

    void HandleEvents()
    {
        // 一定会同时存在大量的fd就绪 可能是listensockfd 可能是普通sockfd
        for (int i = 0; i < gnum; i++)
        {
            if (fd_events[i].fd != defaultarray && (fd_events[i].revents & POLLIN)) // 过滤
            {
                // 读事件就绪 1. listensockfd 2.普通sockfd
                if (_listensock->GetSockfd() == fd_events[i].fd)
                {
                    // listensockfd
                    // 存在说明 已经就绪了
                    HandleAccepter();
                }
                else
                {
                    // 普通sockfd 正常读写
                    HandleIO(i);
                }
            }
        }
    }

    void Loop()
    {
        int timeout = 1000;
        while (true)
        {
            int n = ::poll(fd_events, gnum, timeout);
            if (n > 0)
            {
                sleep(1);
                LOG(INFO, "select success! Some fds have been ready! the count is n: %d\n", n);

                HandleEvents();
                PrintDebug();
            }
            else if (n == 0)
            {

                LOG(WARNING, "wait timeout\n");
            }
            else
            {
                LOG(ERROR, "select error! n: %d\n", n);
            }
        }
    }

    void PrintDebug()
    {
        std::cout << "fd list: ";
        for (int i = 0; i < gnum; i++)
        {
            if (fd_events[i].fd != defaultarray)
                std::cout << fd_events[i].fd << " ";
        }
        std::cout << "\n";
    }

    ~PollServer() {}

private:
    uint16_t _port;
    std::unique_ptr<TcpSocket> _listensock;

    struct pollfd fd_events[gnum]; // 保存合法的 fd
};