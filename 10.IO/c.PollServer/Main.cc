#include <iostream>
#include <memory>

#include "PollServer.hpp"

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cerr << "udpserve argv Usage: " << argv[0] << " serverport" << std::endl;
        exit(-1);
    }

    uint16_t port = std::stoi(argv[1]);

    std::unique_ptr<PollServer> ssvr = std::make_unique<PollServer>(port);
    ssvr->InitServer();
    ssvr->Loop();

    return 0;
}
