#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

int main(void) 
{
	int fd, n;
	char msg[] = "It's a test for lseek\n";
	char ch;
 
	fd = open("lseek.txt", O_RDWR|O_CREAT, 0644);
	if(fd < 0){
		perror("open lseek.txt error");
	  return -1;
	}

 
	write(fd, msg, strlen(msg));    //使用fd对打开的文件进行写操作，问价读写位置位于文件结尾处。
  //        offset  position       return (len) 
	lseek(fd, 0, SEEK_SET);         //修改文件读写指针位置，位于文件开头。 注释该行会怎样呢？
 
	while((n = read(fd, &ch, 1))){
		if(n < 0){
			perror("read error");
			return -1;
		}
		write(STDOUT_FILENO, &ch, n);   //将文件内容按字节读出，写出到屏幕
	}
 
	close(fd);
 
	return 0;

}
//int main()
//{
//  int n;
//  char ch;
//  //char buffer[128];
//  int fd = open("bite", O_CREAT | O_WRONLY, 0664);
//  if(fd == -1)  return -1;
//
//  const char *message = "I live linux!";
//  write(fd, message, strlen(message));
//  lseek(fd, 0, SEEK_SET);
//  //lseek(fd, 0, SEEK_END);
//
//  
//
// while((n = read(fd, &ch, 1))){
//		if(n < 0){
//			perror("read error");
//			return -1;
//		}
//		write(2, &ch, n);   //将文件内容按字节读出，写出到屏幕
//	}
//// read(fd, buffer,strlen(message));
//
// // printf("%s\n", buffer);
//
//  close(fd);
//
//  return 0;
//}
//
//
//
//
//
////int main()
//{
//  char buffer[128];
//  FILE* fp = fopen("bite", "w+");
//  if(NULL == fp)  
//  {
//    perror("fopen fail!");
//    return -1;
//  }
//
//  //write
//  const char *message = "linux so easy!";
//  fwrite(message, 1, strlen(message), fp);
// 
//  //find the head of file
//  fseek(fp, 0, SEEK_SET);
//
//  //read
//  fread(buffer, 1, strlen(message), fp);
//  printf("%s\n", buffer);
//  fclose(fp);
//
//  return 0;
//}
//
//
//





//void func() {
//     int fd = open("./tmp.txt", O_RDWR|O_CREAT, 0664);
//     if (fd < 0) {
//      return;
//     }
//     dup2(fd, 1);
//     printf("hello bit");
//     return;
//}
//int main()
//{
//  func();
//  return 0;
//}



//int main()
//{
//  int cnt = 5;
//  pid_t id = fork();
//  if(0 == id)
//  {
//    //child
//    while(1)
//    {
//      printf("I am child! pid: %d\n", getpid());
//      sleep(2);
//    }
//  }
//  else 
//  {
//
//    while(cnt)
//    {
//      printf("I am father process! pid: %d\n", getpid());
//      sleep(1);
//      cnt--;
//    }
//  }
//
//  return 0;
//}
//
//





//#define Mul(x,y) ++x*++y
//
//int main()
//{
//   // int a = 1;
//   // int b = 2;
//   // int c = 3;
//   // printf("%d", Mul(a + b, b + c));
//    return 0;
//}
