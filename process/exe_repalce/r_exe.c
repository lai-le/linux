#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

int main()
{
  printf("program begin...\n");

  pid_t id = fork();
  if(0 == id)
  {

    printf("child pid:%d\n",getpid());
    sleep(2);
    
    //char* const envp[]=
    //{
    //  (char*)"HHH=11111111111",
    //  (char*)"LLLLLLLL=2222222222222",
    //  NULL
    //};

    char* const argv[]=
    {
      (char*)"inexe",
      (char*)"-a",
      NULL 
    };

    putenv("HHHHH=8888888888888888888888888888888");

    extern char **environ;
    //diao yong de shi dangqian jincheng de environment
    execvpe("./inexe",argv,environ);

    //program replace c++
    //already found inexe
    //execl("/usr/bin/bash","bash","test.sh",NULL);
    //execl("./inexe","inexe",NULL);

   // char *const argv[] = {
   //  (char*)"ls",
   //  (char*)"-l",
   //  (char*)"-a",
   //  (char*)"--color",
   //   NULL
   // };

   // sleep(2);
   // execlp("top","top",NULL);
    //execv("/usr/bin/ls",argv);
    //execv("/usr/bin/ls",argv);
    //execl("/usr/bin/ls","ls","-l","-a",NULL);
    exit(1);
  }

  //father
  int status = 0;
  pid_t rid = waitpid(id,&status,0);
  if(rid > 0)
  {
    printf("Wait success! child exit code: %d\n",WEXITSTATUS(status));

  }

  printf("program end....\n");

  return 0;
}
