#include<stdio.h>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include<stdlib.h>
#include<strings.h>
#include <sys/wait.h>


void ChildRun()
{
 // int *p = NULL;
  int cnt = 5;
  while(cnt)
  {
    printf("I am child,pid:%d,ppid:%d,cnt:%d\n",getpid(),getppid(),cnt);
    sleep(1);
    cnt-- ;
  }
 // *p = 100;
}

int main()
{
  printf("I am father,pid:%d,ppid:%d\n",getpid(),getppid());

  pid_t id = fork();
  if(0 == id)
  {
    //child
    ChildRun();
    printf("child quit...\n");
    exit(321);
  }

  //waitpid<options> test
  //father
  int status = 0;
  pid_t rid = waitpid(id,&status,WNOHANG);
  if(rid == 0)
  {
    printf("chils is running,father check it next time\n");
   // DoOtherThings();
  }
  else if(rid > 0)
  {
    if(WIFEXITED(status))
    {
      printf("sucess! child exit_code: %d\n",WEXITSTATUS(status));
    }
    else
    {
      printf("exit unnormal!\n");
    }
    printf("wai sucess,rid:%d\n",rid);
  }
  else
  {
    printf("wait fail!\n");
  }
  return 0;
}  



//  sleep(7);
//
//  //father
//  int status = 0;
//  pid_t rid = waitpid(id,&status,0);
//  if(rid > 0)
//  {
//    if(WIFEXITED(status))
//    {
//      printf("sucess! child exit_code: %d\n",WEXITSTATUS(status));
//    }
//    else
//    {
//      printf("exit unnormal!");
//    }
//    printf("wai sucess,rid:%d\n",rid);
//  }
//  else
//  {
//    printf("wait fail!\n");
//  }
//  sleep(3);
//  printf("father quit,status:%d,child quit code: %d,child quit signal:%d\n",status,(status >> 8)&0xFF,status & 0x7F);
//
//}
//
//
//









//enum
//{
//  Sucess = 0,
//  Div_Zero,
//  Mod_Zero,
//};
//
//int exit_code = Sucess;
//
//const char* CodeToErrString(int code)
//{
//  switch(code)
//  {
//    case Sucess:
//      return "Sucess";
//    case Div_Zero:
//      return "Div_Zero";
//    case Mod_Zero:
//      return "Mod_Zero";
//    default:
//      return "unknow error";
//  }
//}
//
//int Div(int x,int y)
//{
//  if(0 == y)
//  {
//    exit_code = Div_Zero;
//    return -1;
//  }
//  else
//  {
//    return x / y;
//  }
//}
//
//int main()
//{
//  int result = Div(10,100);
//  printf("result: %d [%s]\n",result,CodeToErrString(exit_code));
//
//  result = Div(10,0);
//  printf("result: %d [%s]\n",result,CodeToErrString(exit_code));
//  return exit_code;
//}
//
//
//
//
//
//



//int main()
//{
//  for(int i = 0;i <= 255;i++)
//  {
//    printf("%d: %s\n",i,strerror(i));
//  }
//  return 0;
//}



//int g_val = 100;
//
//int main(int argc,char* argv[],char* env[])
//{
//    
//  printf("I am father process,pid:%d,ppid:%d,g_val:%d\n",getpid(),getppid(),g_val);
//
//  pid_t id =fork();
//  if(id == 0)
//  {
//    //child
//    int cnt = 0;
//    while(1)
//    {
//       printf("I am child process,pid:%d,ppid:%d,g_val:%d,&g_val:%p\n",getpid(),getppid(),g_val,&g_val);
//       sleep(1);
//       cnt++;
//       if(cnt==5)
//       {
//         g_val = 300;
//         printf("I am child,change");
//       }
//    }
//  }
//  else
//  {
//      while(1)
//      {
//
//       printf("I am father process,pid:%d,ppid:%d,g_val:%d,&g_val:%p\n",getpid(),getppid(),g_val,&g_val);
//       sleep(1);
//      }
//  }
//
//
//  
//  return 0;
//}
//  char *path = getenv("HELLO");
//  if(path == NULL) return 1;
//  printf("path:%s\n",path);
//
//  //for(int i = 0;env[i];i++)
// // {
// //   printf("env[%d]->%s\n",i,env[i]);
//
// // }
//
//  return 0;
//}








//int main()
//{
//
//  extern char** environ;
//  for(int i = 0;environ[i];i++)
//  {
//    printf("env[%d]->%s\n",i,environ[i]);
//  }
//  return 0;
//}

//int g_val = 100000;
//
//int main()
//{
//  printf("I am father process,pid:%d,ppid:%d,g_val:%d\n",getpid(),getppid(),g_val);
//  sleep(5);
//
//  pid_t id =fork();
//  if(id == 0)
//  {
//    //child
//    while(1)
//    {
//       printf("I am child process,pid:%d,ppid:%d,g_val:%d\n",getpid(),getppid(),g_val);
//       sleep(1);
//    }
//  }
//  else
//  {
//      while(1)
//      {
//
//       printf("I am father process,pid:%d,ppid:%d,g_val:%d\n",getpid(),getppid(),g_val);
//       sleep(1);
//      }
//  }
//int main(int argc,char* argv[])
//{
//if(argc != 2)
//{
//  printf("Usage: %s -[a,b,c,d]\n",argv[0]);
//  return 1;
//}
//
//if(strcmp(argv[1],"-a")==0)
//{
//  printf("this is function1\n");
//}
//else if(strcmp(argv[1],"-b")==0)
//{
//  printf("this is function2\n");
//}
//else if(strcmp(argv[1],"-c")==0)
//{
//  printf("this is function3\n");
//}
//else if(strcmp(argv[1],"-d")==0)
//{
//  printf("this is function4\n");
//}
//else
//{
//  printf("no function!!!\n");
//}
//for(int i=0;argv[i];i++)
//{
//  printf("argv[%d]:%s\n",i,argv[i]);
//}

//.autorelabel  return 0;
//}
