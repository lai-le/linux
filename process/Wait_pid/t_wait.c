#include<stdio.h>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include<stdlib.h>
#include<strings.h>
#include <sys/wait.h>
#include "task.h"

typedef void(*func)();

#define N 3
func task[N] = {NULL};

void LoadTask()
{
  task[0] = PrintLog;
  task[1] = DownLoad;
  task[2] = OpenLoadData;
}

void HandleTask()
{
  for(int i = 0;i < N;i++)
  {
    task[i](); //huidiao 
  }
}

void DoOtherThings()
{
  HandleTask();
}

void ChildRun()
{
 // int *p = NULL;
  int cnt = 5;
  while(cnt)
  {
    printf("I am child,pid:%d,ppid:%d,cnt:%d\n",getpid(),getppid(),cnt);
    sleep(2);
    cnt-- ;
  }
 // *p = 100;
}

int main()
{
  printf("I am father,pid:%d,ppid:%d\n",getpid(),getppid());

  pid_t id = fork();
  if(0 == id)
  {
    //child
    ChildRun();
    printf("child quit...\n");
    exit(1);
  }
  
  LoadTask();
  //waitpid<options> test
  //father
  while(1){
    int status = 0;
    pid_t rid = waitpid(id,&status,WNOHANG);
    if(rid == 0)
    {
      sleep(1);
      printf("chils is running,father check it next time\n");
      
      DoOtherThings();
    }
    else if(rid > 0)
    {
      if(WIFEXITED(status))
      {
        printf("sucess! child exit_code: %d\n",WEXITSTATUS(status));
      }
      else
      {
        printf("exit unnormal!\n");
      }
      break;
    }
    else
    {
      printf("wait fail!\n");
      break;
    }
  }
 // return 0;
}  


