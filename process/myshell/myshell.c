#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>

#define SIZE 512
#define NUM 50
#define SEP " "
#define ZERO '\0'
#define SkipPath(p) do{p+=strlen(p)-1;while(*p!='/') p--;}while(0)

int exitcode = 0;
char* g_Argv[NUM];
char cwd[SIZE*2];

void Die()
{
  exit(666);
}
const char* GetUserName()
{
  const char* username = getenv("LOGNAME");
  if(NULL == username)  return "none";
  return username;
}

const char* GetHostName()
{
  const char* hostname = getenv("HOSTNAME");
  if(NULL == hostname)  return "none";
  return hostname;
}

const char* Getcwd()
{
  const char* cwd = getenv("PWD"); 
  if(NULL == cwd) return "none";
  return cwd;
}

void MakeCommandLinePrint(char line[],size_t size)
{
  const char* username = GetUserName();
  const char* hostname = GetHostName();
  const char* cwd = Getcwd();

  SkipPath(cwd);
  snprintf(line,size,"[%s@%s %s]& ",username,hostname,strlen(cwd) == 1 ? "/" : cwd+1);

  printf("%s",line);
  fflush(stdout);
}

int GetUserCommand(char usercommand[],size_t size)
{

  char* s = fgets(usercommand,size,stdin);
  if(s == NULL) return -1;
  usercommand[strlen(usercommand)-1] = ZERO;
  return strlen(usercommand);
}

void SplitCommand(char command[],size_t size)
{
  (void)size;
  g_Argv[0] = strtok(command,SEP);
  int index = 1;
  while((g_Argv[index++] = strtok(NULL,SEP)));
}

void ExecuteCommand()
{
  pid_t id = fork();
  if(id < 0)  Die();
  else if(0 == id)
  {
    //child
    execvp(g_Argv[0],g_Argv);
    exit(errno);
  }
  else 
  {
    //father
    int status = 0;
    pid_t rid = waitpid(id,&status,0);
    if(rid > 0)
    {
      exitcode = WEXITSTATUS(status);
      if(exitcode != 0)
      {
        printf("%s %s:%d\n",g_Argv[0],strerror(exitcode),exitcode);
      }
    }
  }
}

const char* GetHome()
{
  const char* home = getenv("HOME");
  if(NULL == home)  return "/";
  return home;
}

void cd()
{
  const char* path = g_Argv[1];
  if(NULL == path)  path = GetHome();

  //exit
  chdir(path);
   
  char tmp[SIZE*2];
  getcwd(tmp,sizeof(tmp));
  snprintf(cwd,sizeof(cwd),"PWD=%s",tmp);
  putenv(cwd);  
}

int CheckBuildin()
{
  int yes = 0;
  const char* enter_cmd = g_Argv[0];
  if(strcmp(enter_cmd,"cd") == 0)
  {
    yes = 1;
    cd();
  }

  return yes;
}

int main()
{
  int quit = 0;
  while(!quit)
  {
    //1.create a commandline
    char commandline[SIZE];
    MakeCommandLinePrint(commandline,sizeof(commandline));
  
    //2.catch user's command
    char usercommand[SIZE];
    int len = GetUserCommand(usercommand,sizeof(usercommand));
    if(len == 0)  return 1;
  
    //3.split command
    SplitCommand(usercommand,sizeof(usercommand));  
    
    //4.check command is in cmd 
    len = CheckBuildin();

    //5.execute command
    ExecuteCommand();
  }
  return 0;
}
