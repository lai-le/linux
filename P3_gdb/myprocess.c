#include<stdio.h>

int AddTarget(int start,int end)
{
  int i=start;
  int sum=0;
  for(;i<=end;i++)
  {
    sum+=i;
  }

  return sum;
}

int main()
{
  printf("run begin...\n");
  
  int result=0;
  result=AddTarget(1,100);
  printf("result:%d\n",result);

  printf("run end...\n");
  return 0;
}
