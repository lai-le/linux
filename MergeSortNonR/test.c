#include "sort.h"

void Print(int* a,int n)
{
  int i=0;
  for(;i<n;i++)
  {
    printf("%d ",a[i]);
  }

  printf("\n");
}

void Test()
{
  int a[] = {8,10,6,7,1,3,9,4,2};

  Print(a,sizeof(a)/sizeof(int));

  CountSort(a,sizeof(a)/sizeof(int));

 //MergeSortNonR(a,sizeof(a)/sizeof(int));
  Print(a,sizeof(a)/sizeof(int));
}

int main()
{

  Test();

  return 0;
}
