#include "sort.h"

void MergeSortNonR(int* a,int n)
{
  int* tmp = (int*)malloc(sizeof(int)*n);
  if(NULL == tmp)
  {
    perror("malloc fail!");
    return;
  }

  int gap = 1;
  while(gap < n)
  {
  for(int i = 0;i < n;i += gap*2 )
  {
    int begin1 = i,end1 = i + gap - 1;
    int begin2 = i + gap,end2 = i + 2*gap -1; 

    if(begin2 >= n||end1 >= n)
    {
      break;
    }

    if(end2 >= n)
    {
      end2 = n - 1;
    }

    int j = begin1;
    while(begin1 <= end1 && begin2 <= end2)
    {
      if(a[begin1] < a[begin2])
      {
        tmp[j++] = a[begin1++];
      }
      else
      {
        tmp[j++] = a[begin2++];
      }
    }

    while(begin1 <= end1)
    {
      tmp[j++] = a[begin1++];
    }

    while(begin2 <= end2)
    {
      tmp[j++] = a[begin2++];
    }

    memcpy(a + i,tmp + i,sizeof(int) *(end2 - i + 1));

  }
    gap *= 2;

  }
  
  free(tmp);
}


void CountSort(int* a,int n)
{
  int min = a[0];
  int max = a[0];
  
  for(int i = 1;i < n;i++)
  {
    if(a[i] < min)
    {
      min = a[i];
    }

    if(a[i] > max)
    {
      max = a[i];
    }
  }

  int range = max - min + 1;
  int* count = (int*)calloc(range,sizeof(int));

  for(int i = 0;i < n;i++)
  {
    count[a[i] - min]++;
  }

  int i = 0;
  for(int j = 0;j < range;j++)
  {
    while(count[j]--)
    {
      a[i++] =j + min;
    }
  }
}








