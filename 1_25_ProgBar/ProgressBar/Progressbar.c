#include"Progressbar.h"
#include<string.h>
#include<unistd.h>

//void ForTest()
//{
//  printf("this is for test.\n");
//  printf("this is for test.\n");
//  printf("this is for test.\n");
//  printf("this is for test.\n");
//  printf("this is for test.\n");
//  printf("this is for test.\n");
//}

#define length 102
#define style '#'

const char* lable="|/-\\";

//version1
//void ProgBar()
//{
//  char bar[length];
//  memset(bar,'\0',sizeof(bar));
//  int len=strlen(lable);
//
//  int cnt=0;
//  while(cnt<=100)
//  {
//    printf("[%-100s][%3d%%][%c]\r",bar,cnt,lable[cnt%len]);
//    fflush(stdout);
//    bar[cnt++]=style;
//    usleep(20000);
//  }
//
//  printf("\n");
//}
void ProgBar(double total,double current)
{
  char bar[length];
  memset(bar,'\0',sizeof(bar));
  int len=strlen(lable);
  
  int cnt=0;
  double rate=(current*100)/total;
  int loop_count=(int)rate;
  while(cnt<=loop_count)
  {
    printf("[%-100s][%.1lf%%][%c]\r",bar,rate,lable[cnt%len]);
    bar[cnt++]=style;
  }

}
