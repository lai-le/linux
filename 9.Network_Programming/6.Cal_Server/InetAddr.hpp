#pragma once

#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

// 网络ip/port 转化为 本地ip/port
class InetAddr
{
private:
    void ToHost(const struct sockaddr_in &addr)
    {
        _ip = inet_ntoa(addr.sin_addr);
        _port = ntohs(addr.sin_port);
    }

public:
    InetAddr(){}
    InetAddr(const struct sockaddr_in &addr) : _addr(addr)
    {
        ToHost(_addr);
    }

    bool operator!=(const InetAddr &addr)
    {
        return addr._ip != this->_ip || addr._port == this->_port;
    }

    bool operator==(InetAddr &addr)
    {
        // if (addr != *this)  
        return addr._ip == this->_ip && addr._port == this->_port;
    }

    struct sockaddr_in Addr()
    {
        return _addr;
    }

    std::string Ip()
    {
        return _ip;
    }

    uint16_t Port()
    {
        return _port;
    }

    std::string AddrStr()
    {
        return _ip + ": " + std::to_string(_port) ;
    }

    ~InetAddr()
    {
    }

private:
    std::string _ip;
    uint16_t _port;
    struct sockaddr_in _addr;
};