#include <iostream>
#include <string>
#include <jsoncpp/json/json.h>

class Request
{
public:
    Request(){}
    Request(int param_x, int param_y, char oper)
        : _param_x(param_x),
          _param_y(param_y),
          _oper(oper)
    {
    }

    bool serialize(std::string *out)
    {
        Json::Value root;
        root["param_x"] = _param_x;
        root["param_y"] = _param_y;
        root["oper"] = _oper;

        Json::FastWriter writer;
        std::string s = writer.write(root);

        *out = s;
        return true;
    }

    bool Deserialize(const std::string &in)
    {
        Json::Value root;
        Json::Reader reader;
        bool res = reader.parse(in, root);
        if(res == false) return false;

        _param_x = root["param_x"].asInt();
        _param_y = root["param_y"].asInt();
        _oper = root["oper"].asInt();

        return true;
    }

    void SetValue(int param_x, int param_y, char oper)
    {
        _param_x = param_x;
        _param_y = param_y;
        _oper = oper;
    }

    void Print()
    {
        std::cout << _param_x << " " << _param_y << " " << _oper << std::endl;
    }

    ~Request() {}

private:
    int _param_x;
    int _param_y;
    char _oper;
};

int main()
{
    // Request req(111, 222, '+');
    // std::string s;
    // bool flag = req.serialize(&s);

    // std::cout << s << std::endl;

    std::string s = "{\"oper\":43,\"param_x\":111,\"param_y\":222}";
    Request req;
    req.Deserialize(s);
    req.Print();

    return 0;



}