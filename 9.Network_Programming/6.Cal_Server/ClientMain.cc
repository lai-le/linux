#include <iostream>
#include <string>
#include <cstring>
#include <unistd.h>

#include "Protocol.hpp"
#include "Socket.hpp"

using namespace socket_ns;

// ./tcpclient 127.0.0.1 8888
int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        std::cerr << "udpclient argv Usage: " << argv[0] << " serverip serverport" << std::endl;
        exit(-1);
    }

    std::string serverip = argv[1];
    uint16_t serverport = std::stoi(argv[2]);

    SockePtr sock = std::make_shared<TcpSocket>();
    if (sock->BuildClientSocket(serverip, serverport) == false)
    {
        std::cerr << "connect error" << std::endl;
        exit(1);
    }
    std::cout << "connect success!" << std::endl;

    srand(time(nullptr));
    std::string recv_respstr;
    std::string operstring = "+-*/%^&";

    int cnt = 5;
    while (cnt--)
    {
        std::cout << "\n############# client start ##############" << std::endl;
        // 1. 构建数据
        int x = rand() % 10;
        usleep(x * 1000);
        int y = rand() % 10;
        usleep(x * y * 100);
        char oper = operstring[y % operstring.size()];
        std::cout << "construct requestion success!!!!" << std::endl;

        // 2. 构建请求
        // Request req(x, y, oper);
        auto req = Factory::BuildRequestObj();
        req->SetValue(x, y, oper);
        std::cout << "construct requestion success!" << std::endl;

        // 3. 序列化
        std::string reqstr;
        req->serialize(&reqstr);
        std::cout << "serialize success!" << std::endl;

        // 4. 添加报头
        std::string reqjsonstr = EnCode(reqstr);

        std::cout << "request string: \n" << reqstr << std::endl;

        // 5. 发送请求
        sock->Send(reqjsonstr);

        while (true)
        {
            // 6. 读取应答
            ssize_t n = sock->Recv(&recv_respstr);
            if (n <= 0)
            {
                std::cerr << "client receive error!" << std::endl;
                break;
            }

            // 7. 提取完整报文
            std::string recv_respjson = DeCode(recv_respstr);
            if (recv_respjson.empty())
                continue;

            std::cout << "recv_respjson: "  << recv_respjson << std::endl;

            // 8. 反序列化
            auto resp = Factory::BuildResponseObj();
            resp->Deserialize(recv_respjson);

            // 9. 打印结果
            resp->PrintResult();
            break;
        }
        std::cout << "+:43, -:45, *:42, /:47, %:37" << std::endl;
        sleep(1);
    }

    return 0;
}