#pragma once
#include <unistd.h>
#include <string>
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <sys/wait.h>
#include <pthread.h>
#include <functional>

#include "Log.hpp"
#include "InetAddr.hpp"
#include "Socket.hpp"

using namespace log_ns;
using namespace socket_ns;

using service_t = std::function<void(SockePtr sockfd, InetAddr addr)>;

static const uint16_t DEF_PORT = 8888;

class TcpServer
{
public:
    TcpServer(){}
    TcpServer(service_t service, uint16_t port = DEF_PORT)
        : _port(port),
          _listensockfd(std::make_shared<TcpSocket>()),
          _isrunning(false),
          _service(service)
    {
        _listensockfd->BuildListenSocket(_port);
        LOG(INFO, "socket create success, sockfd: %d\n", _listensockfd->GetSockfd()); 
    }

    class ThreadData
    {
    public:
        ThreadData(SockePtr sockfd, InetAddr addr, TcpServer *self)
            : _sockfd(sockfd),
              _addr(addr),
              _self(self)
        {
        }

        ~ThreadData() {}

    public:
        SockePtr _sockfd;
        InetAddr _addr;
        TcpServer *_self;
    };

    void Loop()
    {
        _isrunning = true;
        while (_isrunning)
        {
            //  4. 获取新连接
            InetAddr client;
            SockePtr newsock = _listensockfd->Accepter(&client);
            if (nullptr == newsock)
            {
                LOG(ERROR, "Accept error!\n");
                continue;
            }
            LOG(INFO, "get a new link, sockfd:%d client info: %s\n", newsock->GetSockfd(), client.AddrStr().c_str());

            pthread_t tid;
            ThreadData *td = new ThreadData(newsock, client, this);
            pthread_create(&tid, nullptr, Excute, td);
        }

        _isrunning = false;
    }

    static void *Excute(void *args)
    {
        pthread_detach(pthread_self()); // 线程分离
        ThreadData *td = static_cast<ThreadData *>(args);
        td->_self->_service(td->_sockfd, td->_addr);
        delete td;
        td->_sockfd->CloseSockfd();

        return nullptr;
    }

    ~TcpServer()
    {
    }

private:
    uint16_t _port;
    SockePtr _listensockfd;
    bool _isrunning;
    service_t _service;
};