#pragma once
#include <jsoncpp/json/json.h>
#include <iostream>
#include <string>
#include <memory>

static const std::string sep = "\r\n";

// "len"\r\n"{json}"\r\n
std::string EnCode(std::string jsonpayload)
{
    int len = jsonpayload.size();
    std::string lenstr = std::to_string(len);
    return lenstr + sep + jsonpayload + sep;
}

// "len"\r
// "len"\r\n"{j
// "len"\r\n"{json}"\r\n
// "len"\r\n"{json}"\r\n"le
// "len"\r\n"{json}"\r\n"len"\r\n"{json}"\r\n
std::string DeCode(std::string &packagestreamqueue)
{
    // 如果sep都没有
    auto pos = packagestreamqueue.find(sep);
    if (pos == std::string::npos)
        return std::string();

    // 有sep 提取长度
    std::string lenstr = packagestreamqueue.substr(0, pos);
    int len = std::stoi(lenstr);

    // 如果整串比总长度短 返回空
    int total_len = len + lenstr.size() + 2 * sep.size();
    if (packagestreamqueue.size() < total_len)
        return std::string();

    std::string layload = packagestreamqueue.substr(pos + sep.size(), len);
    packagestreamqueue.erase(0, total_len);
    return layload;
}

class Request
{
public:
    Request() 
    {}
    Request(int param_x, int param_y, char oper)
        : _param_x(param_x),
          _param_y(param_y),
          _oper(oper)
    {}

    bool serialize(std::string *out)
    {
        Json::Value root;
        root["param_x"] = _param_x;
        root["param_y"] = _param_y;
        root["oper"] = _oper;

        Json::FastWriter writer;
        std::string s = writer.write(root);

        *out = s;
        return true;
    }

    bool Deserialize(const std::string &in)
    {
        Json::Value root;
        Json::Reader reader;
        bool res = reader.parse(in, root);
        if (res == false)
            return false;

        _param_x = root["param_x"].asInt();
        _param_y = root["param_y"].asInt();
        _oper = root["oper"].asInt();

        return true;
    }

    void Print()
    {
        std::cout << _param_x << " " << _param_y << " " << _oper << std::endl;
    }

    int GetParam_x()
    {
        return _param_x;
    }

    int GetParam_y()
    {
        return _param_y;
    }

    char GetOper()
    {
        return _oper;
    }

    void SetValue(int param_x, int param_y, char oper)
    {
        _param_x = param_x;
        _param_y = param_y;
        _oper = oper;
    }

    ~Request() {}

private:
    int _param_x;
    int _param_y;
    char _oper;
};

class Response
{
public:
    Response() : _result(0), _code(0), _desc("success")
    {
    }

    bool serialize(std::string *out)
    {
        Json::Value root;
        root["result"] = _result;
        root["code"] = _code;
        root["desc"] = _desc;

        Json::FastWriter writer;
        std::string s = writer.write(root);

        *out = s;
        return true;
    }

    bool Deserialize(const std::string &in)
    {
        Json::Value root;
        Json::Reader reader;
        bool res = reader.parse(in, root);
        if (res == false)
            return false;

        _result = root["result"].asInt();
        _code = root["code"].asInt();
        _desc = root["desc"].asString();

        return true;
    }

    int GetResult()
    {
        return _result;
    }

    void SetResult(int result)
    {
        _result = result;
    }
    int GetCode()
    {
        return _code;
    }
    void SetCode(int code)
    {
        _code = code;
    }
    std::string GetDesc()
    {
        return _desc;
    }
    void SetDesc(std::string desc)
    {
        _desc = desc;
    }

    void PrintResult()
    {
        std::cout << "result: " << _result << ", code:" << _code << ", _desc: " << _desc << std::endl;
    }

    ~Response() {}

private:
    int _result;
    int _code;
    std::string _desc;
};

// 工厂模式
class Factory
{
public:
    static std::shared_ptr<Request> BuildRequestObj()
    {
        return std::make_shared<Request>();
    }

    static std::shared_ptr<Response> BuildResponseObj()
    {
        return std::make_shared<Response>();
    }
};