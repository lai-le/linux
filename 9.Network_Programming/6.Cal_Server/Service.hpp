#pragma once

#include <iostream>
#include <functional>
#include <memory>

#include "InetAddr.hpp"
#include "Socket.hpp"
#include "Protocol.hpp"

using namespace socket_ns;

// using work_t = std::function<std::shared_ptr<Response>(std::shared_ptr<Request>)>;
using work_t = std::function<std::shared_ptr<Response>(std::shared_ptr<Request>)>;

class Service
{
public:
    Service(work_t work) : _work(work)
    {
    }

    void IOExcute(SockePtr sockfd, InetAddr addr)
    {
        std::string message;
        while (true)
        {
            // 1. 接受请求
            ssize_t n = sockfd->Recv(&message);
            if (n < 0)
            {
                LOG(ERROR, "read fail: %s\n", addr.AddrStr().c_str());
                break;
            }
            else if (n == 0)
            {
                LOG(INFO, "client %s quit\n", addr.AddrStr().c_str());
                break;
            }

            std::cout << "---------------- server start----------------" << std::endl;
            std::cout << "message: \n"
                      << message << std::endl;

            // 2. 提取完整报文
            std::string package = DeCode(message);
            if (package.empty())  continue;
            std::cout << "package: \n" << package << std::endl;

            // 3. 反序列化
            auto req = Factory::BuildRequestObj();
            if (req->Deserialize(package) == false)
            {
                std::cerr << "service deserialize fail!" << std::endl;
            }

            // 4. 业务处理
            auto resp = _work(req);
            // std::shared_ptr<Response> resp = _work(req);

            // 5. 序列化
            std::string r_layload;
            resp->serialize(&r_layload);
            std::cout << "respjson: \n" << r_layload << std::endl;

            // 6. 添加报头
            r_layload = EnCode(r_layload);
            std::cout << "respjson add header done: \n" << r_layload << std::endl;

            sockfd->Send(r_layload);
        }
    }

    ~Service()
    {
    }

private:
    work_t _work;
};