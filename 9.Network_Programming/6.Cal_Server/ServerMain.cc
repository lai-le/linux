#include <iostream>
#include <memory>

#include "TcpServer.hpp"
#include "Service.hpp"
#include "Socket.hpp"
#include "NetCal.hpp"

using namespace socket_ns;

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cerr << "udpserve argv Usage: " << argv[0] << " serverport" << std::endl;
        exit(-1);
    }

    uint16_t port = std::stoi(argv[1]);

    Calculator cal;
    Service serv(std::bind(&Calculator::Calculate, &cal, std::placeholders::_1));

    std::unique_ptr<TcpServer> tsvr = std::make_unique<TcpServer>(
        std::bind(&Service::IOExcute, &serv,
        std::placeholders::_1, std::placeholders::_2),
        port);
    // std::unique_ptr<TcpServer> usvr = std::make_unique<TcpServer>(
    //     std::bind(&Command::HandlerCommand, &cmd, std::placeholders::_1, std::placeholders::_2), port);

    tsvr->Loop();

    return 0;
}