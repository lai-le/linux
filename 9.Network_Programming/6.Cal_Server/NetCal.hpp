#pragma once

#include <memory>

#include "Protocol.hpp"

class Calculator
{
public:
    Calculator() {}

    std::shared_ptr<Response> Calculate(std::shared_ptr<Request> req) const
    {
        auto resp = Factory::BuildResponseObj();
        switch (req->GetOper())
        {
        case '+':
            resp->SetResult(req->GetParam_x() + req->GetParam_y());
            break;
        case '-':
            resp->SetResult(req->GetParam_x() - req->GetParam_y());
            break;
        case '*':
            resp->SetResult(req->GetParam_x() * req->GetParam_y());
            break;
        case '/':
        {
            if (req->GetParam_y() == 0)
            {
                resp->SetCode(2);
                resp->SetDesc("divide zero!\n");
            }
            else
            {
                resp->SetResult(req->GetParam_x() / req->GetParam_y());
            }
        }
        break;
        case '%':
        {
            if (req->GetParam_y() == 0 || req->GetParam_x() < 0 || req->GetParam_y() == 0)
            {
                resp->SetCode(2);
                resp->SetDesc("remainder zero!\n");
            }
            else
            {
                resp->SetResult(req->GetParam_x() % req->GetParam_y());
            }
        }
        break;
        default:
        {
            resp->SetCode(3);
            resp->SetDesc("Invalid calculation\n");
            // std::cerr << "Invalid calculation\n"
            //           << std::endl;
        }
        break;
        }

        return resp;
    }

    ~Calculator() {}
};