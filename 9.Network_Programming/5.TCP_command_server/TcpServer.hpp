#pragma once
#include <unistd.h>
#include <string>
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <sys/wait.h>
#include <pthread.h>
#include <functional>

#include "Log.hpp"
#include "InetAddr.hpp"

using namespace log_ns;

// using task_t = std::function<void()>;
using server_t = std::function<void(int sockfd, InetAddr addr)>;

enum
{
    SOCKET_ERROR = 1,
    BIND_ERROR,
    LISTEN_ERROR
};

static const uint16_t DEF_PORT = 8888;
static const int DEF_BACKLOG = 8;
static const int DEF_SOCKFD = -1;

class TcpServer
{
public:
    TcpServer(server_t service, uint16_t port = DEF_PORT)
        : _service(service),
          _port(port),
          _listensockfd(DEF_SOCKFD),
          _isrunning(false)
    {
    }

    void InitServer()
    {
        // 1. 创建socket
        _listensockfd = ::socket(AF_INET, SOCK_STREAM, 0);
        if (_listensockfd < 0)
        {
            LOG(FATAL, "socket create error\n");
            exit(SOCKET_ERROR);
        }
        LOG(INFO, "socket create success, sockfd: %d\n", _listensockfd); // 3

        struct sockaddr_in local;
        memset(&local, 0, sizeof(local));
        local.sin_family = AF_INET;
        local.sin_port = htons(_port);
        local.sin_addr.s_addr = INADDR_ANY;

        // 2. bind sockfd 和 Socket addr
        if (::bind(_listensockfd, (struct sockaddr *)&local, sizeof(local)) < 0)
        {
            LOG(FATAL, "bind error\n");
            exit(BIND_ERROR);
        }
        LOG(INFO, "bind success\n");

        // 3. 因为tcp是面向连接的，tcp需要未来不断地能够做到获取连接
        if (::listen(_listensockfd, DEF_BACKLOG) < 0)
        {
            LOG(FATAL, "listen error\n");
            exit(LISTEN_ERROR);
        }
        LOG(INFO, "listen success\n");
    }

    class ThreadData
    {
    public:
        ThreadData(int sockfd, InetAddr addr, TcpServer *self)
            : _sockfd(sockfd),
              _addr(addr),
              _self(self)
        {
        }

        ~ThreadData() {}

    public:
        int _sockfd;
        InetAddr _addr;
        TcpServer *_self;
    };

    void Loop()
    {
        _isrunning = true;
        while (_isrunning)
        {
            //  4. 获取新连接
            struct sockaddr_in peer;
            socklen_t len = sizeof(peer);

            int sockfd = ::accept(_listensockfd, (struct sockaddr *)&peer, &len);
            if (sockfd < 0) // 不用退出 重新获取
            {
                LOG(ERROR, "Obtain fail! continue \n");
                sleep(10);
                continue;
            }

            //std::cout << "hello" << std::endl;
            InetAddr peer_addr(peer);
            LOG(INFO, "get a new link, sockfd:%d client info: %s\n", sockfd, peer_addr.AddrStr().c_str());

            pthread_t tid;
            ThreadData *td = new ThreadData(sockfd, peer_addr, this);
            pthread_create(&tid, nullptr, Excute, td);
        }

        _isrunning = false;
    }

    static void *Excute(void *args)
    {
        pthread_detach(pthread_self()); // 线程分离
        ThreadData *td = static_cast<ThreadData *>(args);
        td->_self->_service(td->_sockfd, td->_addr); // 执行 回调
        
        delete td;
        return nullptr;
    }


    ~TcpServer()
    {}

private:
    uint16_t _port;
    int _listensockfd; // int _sockfd;

    bool _isrunning;
    server_t _service;
};