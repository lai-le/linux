#include <iostream>
#include <string>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

// ./tcpclient 127.0.0.1 8888
int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        std::cerr << "udpclient argv Usage: " << argv[0] << "serverip serverport" << std::endl;
        exit(-1);
    }

    std::string serverip = argv[1];
    uint16_t serverport = std::stoi(argv[2]);

    // 1. 创建socket
    int sockfd = ::socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        std::cerr << "client socket create fail!\n" << std::endl;
        exit(-1);
    }

    // 不需要显示的 bind
    // 2. connect: If the connection or binding succeeds, zero is returned
    struct sockaddr_in server;
    memset(&server, 0, sizeof(server));
    server.sin_port = htons(serverport);
    server.sin_family = AF_INET;
    ::inet_pton(AF_INET, serverip.c_str(), &server.sin_addr);

    int n = ::connect(sockfd, (struct sockaddr*)&server, sizeof(server));
    if (n < 0)
    {
        std::cerr << "client connect fail!" << std::endl;
        exit(-1);
    }
    else
    {
        std::cout << n << std::endl;
    }

    // connect sucess
    while (true)
    {
        std::string message;
        std::cout << "Enter # ";
        std::getline(std::cin, message);

        std::cout << "send message: " << message << std::endl;
        write(sockfd, message.c_str(), message.size());

        char echo_buffer[1024];
        int ret = read(sockfd, echo_buffer, sizeof(echo_buffer) - 1);
        if (ret > 0)
        {
            echo_buffer[ret] = 0;
            std::cout << echo_buffer << std::endl;
        }
        else
        {
            break;
        }
    }
    ::close(sockfd);

    return 0;
}