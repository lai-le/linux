#pragma once

#include <cstdio>
#include <set>

#include "InetAddr.hpp"

class Command
{
public:
    Command()
    {
        _safe_command.insert("ls");
        _safe_command.insert("which");
        _safe_command.insert("touch");
        _safe_command.insert("pwd");
        _safe_command.insert("whoami");
    }

    bool SafeCheck(const std::string &command)
    {
        for (auto &cmd : _safe_command)
        {
            if (strncmp(cmd.c_str(), command.c_str(), cmd.size()) == 0)
            {
                return true;
            }
        }

        return false;
    }

    std::string Excute(const std::string &command)
    {
        if (SafeCheck(command) == false)
            return "invaild command!";

        std::string result;
        FILE *fp = ::popen(command.c_str(), "r"); // 远端执行返回 pipe文件指针(里面结果)
        if (nullptr == fp)
        {
            return "command error!\n";
        }

        char line[1024];
        while (::fgets(line, sizeof(line), fp)) // 从fp 读sizeof(line) 读到 line
        {
            result += line;
        }

        return result.empty() ? "sucess" : result;
    }

    void HandlerCommand(int sockfd, InetAddr addr)
    {
        // 长服务~~~~~~~
        while (true)
        {
            char command[1024];
            ssize_t n = ::read(sockfd, command, sizeof(command) - 1);
            if (n > 0)
            {
                command[n] = 0;
                std::cout << "server receive command: " << command << std::endl;

                std::string result = Excute(command);
                ::send(sockfd, result.c_str(), result.size(), 0);
            }
            else if (n == 0)
            {
                LOG(INFO, "client %s quit\n", addr.AddrStr().c_str());
                break;
            }
            else
            {
                LOG(ERROR, "read fail: %s\n", addr.AddrStr().c_str());
                break;
            }
        }

        ::close(sockfd);
    }

    ~Command() {}

private:
    std::set<std::string> _safe_command;
};