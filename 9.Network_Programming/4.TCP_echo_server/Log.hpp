#pragma once

#include <iostream>
#include <string>
#include <unistd.h>
#include <sys/types.h>
#include <time.h>
#include <stdarg.h>
#include <fstream>
#include <cstring>

namespace log_ns
{
    enum
    {
        DEBUGE = 1,
        INFO,
        WARNING,
        ERROR,
        FATAL
    };

    class LogMessage // 日志消息
    {
    public:
        std::string _level;
        pid_t _id;
        std::string _filename;
        size_t _filenumber;
        std::string _currtime;
        std::string _info;
    };

    std::string LevelToString(int level)
    {
        switch (level)
        {
        case DEBUGE:
            return "DEBUGE";
        case INFO:
            return "INFO";
        case WARNING:
            return "WARNING";
        case ERROR:
            return "ERROR";
        case FATAL:
            return "FATAL";
        default:
            return "UNKNOW";
        }
    }

    std::string GetCurrTime()
    {
        time_t tloc;
        time(&tloc); // 获取时间戳
        // std::cout << tloc << std::endl;

        struct tm *curr_time = localtime(&tloc);
        char buffer[128];
        snprintf(buffer, sizeof(buffer), "%d-%02d-%02d %02d:%02d:%02d", curr_time->tm_year + 1900, curr_time->tm_mon + 1, curr_time->tm_mday, curr_time->tm_hour, curr_time->tm_min, curr_time->tm_sec);

        return buffer;
    }

    const std::string DEFAULTFILE = "./log.txt";
    const int SCREEN_TYPE = 1;
    const int FILE_TYPE = 2;

    // log.LogRowMessage(DEBUGE, id, "filename", "filenumber", "time", "content")
    class Log // 日志
    {
    public:
        Log(const std::string &defaultfile = DEFAULTFILE) : _ptype(SCREEN_TYPE), _defaultfile(defaultfile)
        {
        }

        void ShifType(int type)
        {
            _ptype = type;
        }

        void FlushLogToScreen(const LogMessage &lg)
        {
            printf("[%s][%d][%s][%ld][%s] %s", lg._level.c_str(), lg._id, lg._filename.c_str(), lg._filenumber, lg._currtime.c_str(), lg._info.c_str());
        }

        void FlushLogToFile(const LogMessage &lg)
        {
            std::ofstream out(_defaultfile, std::ios::app);
            if (!out.is_open())
                return;
            char logtxt[2048];
            snprintf(logtxt, sizeof(logtxt), "[%s][%d][%s][%ld][%s] %s", lg._level.c_str(), lg._id, lg._filename.c_str(), lg._filenumber, lg._currtime.c_str(), lg._info.c_str());
            out.write(logtxt, strlen(logtxt));
            out.close();
        }

        void FlushLog(const LogMessage &lg)
        {
            switch (_ptype)
            {
            case SCREEN_TYPE:
                FlushLogToScreen(lg);
                break;
            case FILE_TYPE:
                FlushLogToFile(lg);
                break;
            default:
                break;
            }
        }

        void LogRowMessage(const std::string &filename, int filenumber, int level, const char *format, ...)
        {
            LogMessage lg;

            lg._level = LevelToString(level);
            lg._id = getpid();
            lg._filename = filename;
            lg._filenumber = filenumber;
            lg._currtime = GetCurrTime();

            va_list ap;
            va_start(ap, format);
            char log_info[1024];
            vsnprintf(log_info, sizeof(log_info), format, ap);
            va_end(ap);
            lg._info = log_info;

            // 打印出来日志
            FlushLog(lg);

            // std::cout << lg._info << std::endl;
            // std::cout << lg._currtime << std::endl;
        }

        ~Log()
        {
        }

    private:
        int _ptype;
        std::string _defaultfile;
    };

    Log lg;
#define LOG(level, format, ...)                                                                  \
    do                                                                                           \
    {                                                                                            \
        lg.LogRowMessage(__FILE__, __LINE__, level, format, ##__VA_ARGS__ /*##支持不传此参数*/); \
    } while (0)

#define SHIFTSCREEN()             \
    do                            \
    {                             \
        lg.ShifType(SCREEN_TYPE); \
    } while (0)
#define SHIFTFILE()             \
    do                          \
    {                           \
        lg.ShifType(FILE_TYPE); \
    } while (0)
}
