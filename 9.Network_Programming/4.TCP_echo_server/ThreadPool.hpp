#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <pthread.h>
#include <functional>
#include "Thread.hpp"
#include "Log.hpp"
#include "LockGuard.hpp"

using namespace ThreadMoudle;
using namespace log_ns;

const static size_t THREADNUM = 5;

// void test()
// {
//     std::cout << "test test test ..." << std::endl;
// }

template <typename T>
class ThreadPool
{
private:
    void LockQueue()
    {
        pthread_mutex_lock(&_mutex);
    }
    void UnLockQueue()
    {
        pthread_mutex_unlock(&_mutex);
    }
    void Wait()
    {
        pthread_cond_wait(&_cond, &_mutex);
    }

    void WakeUp()
    {
        pthread_cond_signal(&_cond);
    }

    void WakeUpAll()
    {
        pthread_cond_broadcast(&_cond);
    }

    bool IsEmpty()
    {
        return _task_queue.empty();
    }

    void HandleTask(const std::string &name) // 隐含 this*
    {
        while (true)
        {
            LockQueue();
            while (IsEmpty() && _isrunning) // 没有任务 && 线程池不退出
            {
                _sleepthread_num++;
                LOG(INFO, "%s thread begin wait\n", name.c_str());

                Wait();
                LOG(INFO, "%s thread wakeup\n", name.c_str());

                _sleepthread_num--;
            }
            // 判定一种情况 需要退出
            if (IsEmpty() && !_isrunning)
            {
                // std::cout << name << "quit.." << std::endl;
                LOG(INFO, "%s thread quit\n", name.c_str());
                UnLockQueue();
                break;
            }

            // 1. 有任务 || 2. 被唤醒
            T t = _task_queue.front();
            _task_queue.pop();
            UnLockQueue();

            // 处理任务 不能放在临界区里面 处理任务可能很耗时间
            // 任务属于线程 不属于队列了
            t();

            // LOG(DEBUGE, "handle task done, task is %d\n", t.getResult());

            // std::cout << name << " result: " << t.getResult() << std::endl;
        }
    }

    ThreadPool(size_t thread_num = THREADNUM)
        : _thread_num(thread_num), _isrunning(false), _sleepthread_num(0)
    {
        pthread_mutex_init(&_mutex, nullptr);
        pthread_cond_init(&_cond, nullptr);
    }

    ThreadPool(const ThreadPool<T> &tp) = delete;
    void operator=(const ThreadPool &tp) = delete;

    void Init()
    {
        // bind 有作用体现在类 模块相互调用 placeholders::_1为自己传的参数
        func_t func = std::bind(&ThreadPool::HandleTask, this, std::placeholders::_1);
        for (int i = 0; i < THREADNUM; i++)
        {
            std::string name = "thread-" + std::to_string(i + 1);
            _threads.emplace_back(name, func);
            LOG(DEBUGE, "construct %s thread done, init success\n", name.c_str());
        }
    }

    void Start()
    {
        _isrunning = true;
        for (auto &thread : _threads) // 注意加引用
        {
            LOG(DEBUGE, "start thead %s done\n", thread.getName().c_str());
            thread.start();
        }
    }

public:
    void Stop()
    {

        LockQueue();
        _isrunning = false;

        WakeUpAll(); // 可能全部处于休眠状态 需要唤醒所有线程
        UnLockQueue();
        LOG(INFO, "thread pool stop success\n");
    }

    void Equeue(const T &in)
    {
        LockQueue();
        if (_isrunning == true) //
        {
            _task_queue.push(in);
            if (_sleepthread_num > 0)
            {
                WakeUp();
            }
        }
        UnLockQueue();
    }

    static ThreadPool<T> *GetInstance() // 调用了静态成员 保证自己也是静态的
    {
        if (_tp == nullptr)
        {
            LockGuard lockguard(&_sig_mutex);
            if (_tp == nullptr)
            {
                LOG(INFO, "create threadpool\n");

                _tp = new ThreadPool<T>();
                _tp->Init();
                _tp->Start();
            }
            else
            {
                LOG(INFO, "get threadpool\n");
            }
        }

        return _tp;
    }

    ~ThreadPool()
    {
        pthread_mutex_destroy(&_mutex);
        pthread_cond_destroy(&_cond);
    }

private:
    std::vector<Thread> _threads;
    std::queue<T> _task_queue;
    size_t _thread_num;
    bool _isrunning;

    size_t _sleepthread_num; // 睡眠线程

    pthread_cond_t _cond;
    pthread_mutex_t _mutex;

    // 单例模式
    static ThreadPool<T> *_tp;
    static pthread_mutex_t _sig_mutex;
};

template <class T>
ThreadPool<T> *ThreadPool<T>::_tp = nullptr; // 初始化

template <class T>
pthread_mutex_t ThreadPool<T>::_sig_mutex = PTHREAD_MUTEX_INITIALIZER;