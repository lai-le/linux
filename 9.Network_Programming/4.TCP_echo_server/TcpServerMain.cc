#include <iostream>
#include <memory>

#include "TcpServer.hpp"

// ./udpserver 8888
int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cerr << "udpserve argv Usage: " << argv[0] << " serverport" << std::endl;
        exit(-1);
    }

    uint16_t port = std::stoi(argv[1]);

    std::unique_ptr<TcpServer> usvr = std::make_unique<TcpServer>(port);
    usvr->InitServer();
    usvr->Loop();

    return 0;
}