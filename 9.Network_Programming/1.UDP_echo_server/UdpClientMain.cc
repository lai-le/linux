#include <iostream>
#include <string>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

// ./udpclient 127.0.0.1 8899
int main(int argc, char* argv[])
{
    if(argc != 3)
    {
        std::cerr << "udpclient argv Usage: " << argv[0] << "serverip serverport" << std::endl;
        exit(-1); 
    }

    // 1. 创建socket
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(sockfd < 0)
    {
        std::cerr << "client socket create fail!\n" << std::endl;
        exit(-1);
    }

    // 2.bind client不需要显示bind
    
    std::string ip = argv[1];
    uint16_t port = std::stoi(argv[2]);
    struct sockaddr_in dest_addr;
    memset(&dest_addr, 0, sizeof(dest_addr));
    dest_addr.sin_family = AF_INET;
    dest_addr.sin_port = htons(port);
    dest_addr.sin_addr.s_addr = inet_addr(ip.c_str());

    while(1)
    {
        std::string line;
        std::cout << "Please enter# ";
        std::getline(std::cin, line);

        int n = sendto(sockfd, line.c_str(), line.size(), 0, (struct sockaddr*)&dest_addr, sizeof(dest_addr));
        if(n > 0)
        {
            char buffer[1028];
            struct sockaddr_in temp;
            socklen_t len = sizeof(temp);
            ssize_t r_n = recvfrom(sockfd, buffer, sizeof(buffer), 0, (struct sockaddr*)&temp, &len);
            if(r_n < 0)
            {
                std::cerr << "client recvfrom fail!\n" << std::endl;
                exit(-1); 
            }
            std::cout << buffer << std::endl;
        }
        else
        {
            std::cerr << "client send fail!\n" << std::endl;
            break;
        }
    }   

    return 0;
}