#include <iostream>
#include <unistd.h>
#include <memory>
#include <string>

#include "Route.hpp"
#include "UdpServer.hpp"
using namespace log_ns;

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cerr << "udpserve argv Usage: " << argv[0] << "serverport" << std::endl;
        exit(-1);
    }
    uint16_t port = std::stoi(argv[1]);

    Route messageroute;
    server_t message_route = std::bind(&Route::Forward, &messageroute, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);

    SHIFTSCREEN();
    std::unique_ptr<UdpServer> usvr = std::make_unique<UdpServer>(message_route ,port); // c++14
    usvr->InitServer();
    usvr->Start();

    return 0;
}