#pragma once

#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstring>
#include <string>
#include <unistd.h>
#include <functional>

#include "nocopy.hpp"
#include "Log.hpp"
#include "InetAddr.hpp"

using namespace log_ns;

using server_t = std::function<void(int, std::string, InetAddr&)>;

static const int GSOCKFD = -1;
static const uint16_t DEF_PORT = 8888;

enum
{
    SOCKET_ERROR = 1
};

class UdpServer : public NoCopy
{
public:
    UdpServer(server_t func, uint16_t localport = DEF_PORT)
        : _sockfd(GSOCKFD),
          _localport(localport),
          _isrunning(false),
          _func(func)
    {
    }

    void InitServer()
    {
        // 1. 创建 socket
        _sockfd = ::socket(AF_INET, SOCK_DGRAM, 0);
        if (_sockfd < 0)
        {
            LOG(FATAL, "socket create fail!\n");
            exit(SOCKET_ERROR);
        }
        LOG(DEBUGE, "socket create success, _socket: %d\n", _sockfd); // 3

        // 2.bind
        struct sockaddr_in local;
        memset(&local, 0, sizeof(local));
        local.sin_family = AF_INET;
        // local.sin_addr.s_addr = inet_addr(_localip.c_str()); // 1.4字节ip 2.网络序列ip
        local.sin_addr.s_addr = INADDR_ANY; // 服务器绑定任意ip 0
        local.sin_port = htons(_localport);

        int n = ::bind(_sockfd, (struct sockaddr *)&local, sizeof(local));
        if (n < 0)
        {
            LOG(FATAL, "socket bind fail!\n");
            exit(SOCKET_ERROR);
        }
        LOG(DEBUGE, "bind create success\n");
    }

    void Start()
    {
        _isrunning = true;
        char inbuffer[1024];
        while(_isrunning)
        {
            struct sockaddr_in src_addr;
            socklen_t addrlen = sizeof(src_addr);
            ssize_t n = recvfrom(_sockfd, inbuffer, sizeof(inbuffer) - 1, 0, (struct sockaddr*)&src_addr, &addrlen);
            if(n > 0)
            {
                InetAddr addr(src_addr);

                inbuffer[n] = 0;
                std::cout << "[" << addr.Ip() << ": " << addr.Port() << "]" << inbuffer << std::endl;

                LOG(DEBUGE, "[%s]# \n", addr.AddrStr().c_str());

                _func(_sockfd, inbuffer, addr); // 回调
                LOG(DEBUGE, "return udpserver.\n");
            }
        }
    }

    ~UdpServer() 
    {   
        if(_sockfd > GSOCKFD)
        {
            ::close(_sockfd);
        }
    }

private:
    int _sockfd; // 读写用一个sockfd, UDP全双工通信的
    uint16_t _localport;
    // std::string _localip;
    bool _isrunning;

    server_t _func;
};