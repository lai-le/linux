#pragma once

#include <vector>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <functional>
#include <mutex>

#include "InetAddr.hpp"
#include "ThreadPool.hpp"
#include "Log.hpp"
#include "LockGuard.hpp"

using namespace log_ns;

using task_t = std::function<void()>;

class Route
{
public:
    Route()
    {
        pthread_mutex_init(&_mutex, nullptr);
    }

    void CheckOnlineUser(InetAddr &who) 
    {
        LockGuard lockguard(&_mutex);
        for (auto &user : _online_users) // _online_users 会有线程安全的问题 加锁保护 (eg:有人在forward有人在check)
        {
            if (who == user)
            {
                LOG(DEBUGE, "%s is exists\n", who.AddrStr().c_str());
                return;
            }
        }

        LOG(DEBUGE, "%s is not exists, add it\n", who.AddrStr().c_str());
        _online_users.push_back(who);
    }

    // 下线
    void OffLine(InetAddr &who)
    {
        LockGuard lockguard(&_mutex);

        auto iter = _online_users.begin();

        while (iter != _online_users.end())
        {
            if (who == *iter)
            {
                LOG(DEBUGE, "%s is offline\n", who.AddrStr().c_str());

                _online_users.erase(iter);
                break;
            }
            iter++;
        }
    }

    void ForwardHelper(int sockfd, const std::string message, InetAddr who)
    {
        LockGuard lockguard(&_mutex);

        std::string send_message = "[" + who.AddrStr() + "]" + message; // 带上标识 也可带名字 头像
        for (auto &user : _online_users)
        {
            LOG(DEBUGE, "%s Forward message, message is %s\n", user.AddrStr().c_str(), send_message.c_str());

            struct sockaddr_in src_addr = user.Addr(); // 获取保存的
            sendto(sockfd, send_message.c_str(), send_message.size(), 0, (struct sockaddr *)&src_addr, sizeof(src_addr));
        }
    }

    void Forward(int sockfd, const std::string &message, InetAddr &who)
    {
        // 1. 判断该用户是否在在线用户列表
        CheckOnlineUser(who);

        // 1.1 用户输入 “QUIT” 或者 “Q” 下线
        if (message == "QUIT" || message == "Q")
        {
            OffLine(who);
        }

        // 2. 一定在 online_users
        // ForwardHelper(sockfd, message);
        task_t task = std::bind(&Route::ForwardHelper, this, sockfd, message, who);
        ThreadPool<task_t>::GetInstance()->Equeue(task);
    }

    ~Route()
    {
        pthread_mutex_destroy(&_mutex);
    }

private:
    std::vector<InetAddr> _online_users; // 线程安全
    pthread_mutex_t _mutex;
};