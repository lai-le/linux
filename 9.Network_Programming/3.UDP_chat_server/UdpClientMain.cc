#include <iostream>
#include <string>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "Thread.hpp"

using namespace ThreadMoudle;

int InitClient()
{
    // 1. 创建socket
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
    {
        std::cerr << "client socket create fail!\n"
                  << std::endl;
        exit(-1);
    }

    return sockfd;
}

void ReceiveMess(int sockfd, const std::string &name)
{
    while (true)
    {
        char buffer[1028];
        struct sockaddr_in temp;
        socklen_t len = sizeof(temp);

        ssize_t n = recvfrom(sockfd, buffer, sizeof(buffer) - 1, 0, (struct sockaddr *)&temp, &len);
        if (n < 0)
        {
            std::cerr << "client recvfrom fail!\n" << std::endl;
            exit(-1);
        }

        buffer[n] = 0;
        std::cerr << buffer << std::endl; // 打印到不同显示器 
    }
}

void SendMess(int sockfd, std::string serverip, uint16_t serverport, const std::string &name)
{
    struct sockaddr_in dest_addr;
    memset(&dest_addr, 0, sizeof(dest_addr));
    dest_addr.sin_family = AF_INET;
    dest_addr.sin_port = htons(serverport);
    dest_addr.sin_addr.s_addr = inet_addr(serverip.c_str());

    std::string client_prompt = name + "# "; // sender-thread# "hello"
    while (true)
    {
        std::string line;
        std::cout << client_prompt;
        std::getline(std::cin, line);

        int n = sendto(sockfd, line.c_str(), line.size(), 0, (struct sockaddr *)&dest_addr, sizeof(dest_addr));
        if (n <= 0)
        {
            std::cerr << "client sent fail." << std::endl;
            break;
        }
    }
}

// ./udpclient 127.0.0.1 8899
int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        std::cerr << "udpclient argv Usage: " << argv[0] << "serverip serverport" << std::endl;
        exit(-1);
    }

    std::string serverip = argv[1];
    uint16_t serverport = std::stoi(argv[2]);
    int sockfd = InitClient();

    //         // sockfd 可读可写
    //         // 如果键盘不输入 就不调用recvfrom 也就收不到 如何处理？ 所以客户端也要多线程
    //         // 一个收 显示到另一个显示器； 一个发 获取数据+显示终端

    Thread receiver("receiver-thread", std::bind(&ReceiveMess, sockfd, std::placeholders::_1));
    Thread sender("sender-thread", std::bind(&SendMess, sockfd, serverip, serverport, std::placeholders::_1));

    receiver.start();
    sender.start();

    receiver.join();
    sender.join();

    ::close(sockfd);
    return 0;
}

    // 2.bind client不需要显示bind

    // struct sockaddr_in dest_addr;
    // memset(&dest_addr, 0, sizeof(dest_addr));
    // dest_addr.sin_family = AF_INET;
    // dest_addr.sin_port = htons(port);
    // dest_addr.sin_addr.s_addr = inet_addr(ip.c_str());

    // while (1)
    // {
    //     std::string line;
    //     std::cout << "Please enter# ";
    //     std::getline(std::cin, line);

    //     int n = sendto(sockfd, line.c_str(), line.size(), 0, (struct sockaddr *)&dest_addr, sizeof(dest_addr));
    //     if (n > 0)
    //     {
    //         char buffer[1028];
    //         struct sockaddr_in temp;
    //         socklen_t len = sizeof(temp);
    //         // sockfd 可读可写
    //         // 如果键盘不输入 就不调用recvfrom 也就收不到 如何处理？ 所以客户端也要多线程
    //         // 一个收 显示到另一个显示器； 一个发 获取数据+显示终端
    //         ssize_t r_n = recvfrom(sockfd, buffer, sizeof(buffer) - 1, 0, (struct sockaddr *)&temp, &len);
    //         if (r_n < 0)
    //         {
    //             std::cerr << "client recvfrom fail!\n"
    //                       << std::endl;
    //             exit(-1);
    //         }
    //         buffer[n] = 0;
    //         std::cout << buffer << std::endl;
    //     }
    //     else
    //     {
    //         std::cerr << "client send fail!\n"
    //                   << std::endl;
    //         break;
    //     }
    // }