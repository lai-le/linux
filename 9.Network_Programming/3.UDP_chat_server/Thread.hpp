#include <iostream>
#include <string>
#include <pthread.h>
#include <functional>

namespace ThreadMoudle
{
    // typedef void (*func_t)(ThreadData *td);
    using func_t = std::function<void(const std::string &name)>;

    class Thread
    {
    public:
        void Excute()
        {
            //std::cout << _name << " is running" << std::endl;
            _isrunning = true;
            _func(_name);
            _isrunning = false;
            
        }

    public:
        Thread(const std::string &name, func_t func)
            :_name(name)
            ,_func(func)
        {
            //std::cout << "create " << name << " done" << std::endl;
        }
        static void *ThreadRoutine(void *args) // 隐藏参数 this指针 static解决
        {
            // _func(_name); 没有this指针 所以调不动 传递this解决
            Thread *self = static_cast<Thread*>(args);
            //self->_func(self->_name); // 不够优雅
            self->Excute(); // 方便调试

            return nullptr;
        }

        std::string status()
        {
            if(_isrunning == true)  return "running";
            else return "sleep";
        }

        bool start()
        {
            int n = ::pthread_create(&_tid, nullptr, ThreadRoutine, this);
            if(n != 0)  return false;
            else return true;
        }
        void stop()
        {
            if(_isrunning)
            {
                _isrunning = false;
                ::pthread_cancel(_tid);
            }
            //std::cout << _name << "is stopping..." << std::endl;
        }
        void join()
        {
            ::pthread_join(_tid, nullptr);
            //std::cout << _name << " is joined..." << std::endl;
        }
        std::string getName()
        {
            return _name;
        }

        ~Thread(){}

    private:
        std::string _name;
        pthread_t _tid;
        bool _isrunning;
        func_t _func; //new thread要执行的函数
    };
}