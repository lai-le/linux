#pragma once

// 防拷贝 作为基类
class NoCopy
{
public:
    NoCopy(){}
    NoCopy(const NoCopy&) = delete;
    NoCopy& operator=(const NoCopy&) = delete;
    ~NoCopy(){}
};