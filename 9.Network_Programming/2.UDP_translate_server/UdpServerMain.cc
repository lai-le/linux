#include <iostream>
#include <unistd.h>
#include <memory>
#include <string>

#include "Dict.hpp"
#include "UdpServer.hpp"
using namespace log_ns;

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cerr << "udpserve argv Usage: " << argv[0] << "serverport" << std::endl;
        exit(-1);
    }
    uint16_t port = std::stoi(argv[1]);

    // std::string ip = "127.0.0.1";
    // uint16_t port = 8899;

    Dict dict("./Dict.txt");
    func_t translate = std::bind(&Dict::Translate, &dict, std::placeholders::_1); // 绑定 有this指针

    SHIFTSCREEN();
    std::unique_ptr<UdpServer> usvr = std::make_unique<UdpServer>(translate, port); // c++14
    usvr->InitServer();
    usvr->Start();

    return 0;
}