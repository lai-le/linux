#pragma once

#include <string>
#include <iostream>
#include <vector>
#include <sstream>
#include <fstream>
#include <functional>
#include <unordered_map>

#include "Log.hpp"

using namespace log_ns;

const static std::string newline_sep = "\r\n";     // 报头换行
const static std::string header_kv_sep = ": ";     // 报头headers kv 分割
const static std::string one_sep = " ";            // 报头空格
const static std::string prefixpath = "wwwroot";   // 文件前缀 web根目录
const static std::string indexpage = "index.html"; // 首页
const static std::string httpversion = "HTTP/1.1";
const static std::string def_suffix = ".default"; // 默认后缀
const static std::string last_point = ".";        // 后缀的点
const static std::string get_param_sep = "?";     // get参数 ？分隔符

class HttpRequest
{
private:
    std::string GetLine(std::string &reqstr)
    {
        auto pos = reqstr.find(newline_sep);
        if (pos == std::string::npos)
            return "";

        std::string info = reqstr.substr(0, pos);
        reqstr.erase(0, info.size() + newline_sep.size());
        if (info.empty())
            return newline_sep;

        // return "";

        return info;
    }

    void ObtainReqLineMess()
    {
        std::stringstream ss(_req_line);
        ss >> _method >> _url >> _version; // /login?username=zhangsan

        // 如果GET带参数 提取参数
        if (strcasecmp(_method.c_str(), "GET") == 0) // 忽略大小写比较
        {
            auto pos = _url.find(get_param_sep);
            if (pos != std::string::npos)
            {
                _req_content = _url.substr(pos + get_param_sep.size());
                _url.resize(pos);
            }
        }

        _path += _url;

        if (_path[_path.size() - 1] == '/')
        {
            _path += indexpage;
        }

        // wwwroot/index.html
        auto pos = _path.rfind(last_point);
        if (pos != std::string::npos)
        {
            _suffix = _path.substr(pos);
        }
        else
        {
            _suffix = ".default";
        }

        //         // wwwroot/index.html
        //         // wwwroot/image/1.png
        //         auto pos = _path.rfind(suffixsep);
        //         if (pos != std::string::npos)
        //         {
        //             _suffix = _path.substr(pos);
        //         }
        //         else
        //         {
        //             _suffix = ".default";
        //         }
    }

    void ObtainReqHeaderKvMess()
    {
        for (auto &header : _req_headers)
        {
            auto pos = header.find(header_kv_sep);
            if (pos == std::string::npos)
                continue;

            std::string key = header.substr(0, pos);
            std::string value = header.substr(pos + header_kv_sep.size());
            if (key == "" || value == "")
                continue;
            ;

            _headers_kv.emplace(key, value);
        }
    }

public:
    HttpRequest() : _blank_line(newline_sep), _path(prefixpath), _suffix(def_suffix)
    {
    }

    void Deserialize(std::string &reqstr)
    {
        _req_line = GetLine(reqstr);

        std::string header;
        do
        {
            header = GetLine(reqstr);
            if (header.empty())
                break;
            else if (header == newline_sep)
                break; // 让他等于空字符串 好像也能成功 待会在做实验 已完成成功
            _req_headers.emplace_back(header);

        } while (true);

        if (!reqstr.empty())
        {
            _req_content = reqstr;
        }

        ObtainReqLineMess();
        ObtainReqHeaderKvMess();
    }

    std::string GetUrl()
    {
        LOG(DEBUGE, "client want url:%s\n", _url.c_str());
        return _url;
    }

    std::string GetPath()
    {
        LOG(DEBUGE, "client want path:%s\n", _path.c_str());

        return _path;
    }

    std::string GetSuffix()
    {
        return _suffix;
    }

    std::string GetMethod()
    {
        LOG(DEBUGE, "client request method: %s\n", _method.c_str());
        return _method;
    }

    std::string GetReqContent()
    {
        LOG(DEBUGE, "client request method: %s, param: %s, request path: %s",
            _method.c_str(), _req_content.c_str(), _path.c_str());
        return _req_content;
    }

    void PrintReq()
    {
        std::cout << "###" << _req_line << std::endl;
        for (auto &header : _req_headers)
        {
            std::cout << "***" << header << std::endl;
        }
        std::cout << "---" << _blank_line;
        std::cout << "+++" << _req_content << std::endl;

        // std::cout << _method << one_sep << _url << one_sep << _version << std::endl;
        // for(auto &kv : _headers_kv)
        // {
        //     std::cout << kv.first << header_kv_sep << kv.second << std::endl;
        // }
    }

    ~HttpRequest() {}

private:
    // 获取基本属性的信息 属性
    std::string _method;
    std::string _url;
    std::string _path;   // 真实资源 路径
    std::string _suffix; // 资源后缀
    std::string _version;
    std::unordered_map<std::string, std::string> _headers_kv;

    // http请求 基本属性
    std::string _req_line;
    std::vector<std::string> _req_headers;
    std::string _blank_line;
    std::string _req_content;
};

class HttpRespond
{
public:
    HttpRespond() : _version(httpversion), _blank_line(newline_sep)
    {
    }

    void AddCode(int status_code, const std::string &desc)
    {
        _status_code = status_code;
        _desc = desc;
    }

    void AddHeadersKv(const std::string &key, const std::string &value)
    {
        _headers_kv[key] = value;
    }

    void AddContent(const std::string &content)
    {
        _resp_content = content;
    }

    // 应答序列化
    std::string Serialize()
    {
        // 1. 构建状态行
        _status_line = _version + one_sep + std::to_string(_status_code) + one_sep + _desc + newline_sep;

        // 2. 应答报头
        for (auto &kv : _headers_kv)
        {
            std::string header = kv.first + header_kv_sep + kv.second + newline_sep;
            _resp_headers.emplace_back(header);
        }

        // 3. 添加 空行和正文

        // 4. 正式序列化
        std::string respondstr = _status_line;
        for (auto &header : _resp_headers)
        {
            respondstr += header;
            // respondstr += newline_sep;
        }

        respondstr += _blank_line;
        respondstr += _resp_content;

        return respondstr;
    }

    void PrintResp()
    {
        std::cout << _version << one_sep << _status_code << one_sep << _desc << std::endl;
        for (auto &kv : _headers_kv)
        {
            std::cout << kv.first << header_kv_sep << kv.second << std::endl;
        }
    }

    ~HttpRespond() {}

private:
    // 获取基本属性的信息 属性
    std::string _version;
    int _status_code;
    std::string _desc; // 状态码描述
    std::unordered_map<std::string, std::string> _headers_kv;

    // http 应答基本属性
    std::string _status_line;
    std::vector<std::string> _resp_headers;
    std::string _blank_line;
    std::string _resp_content;
};

using func_t = std::function<HttpRespond(HttpRequest&)>;

class HttpServer
{
private:
    std::string GetFileContent(const std::string &path)
    {
        std::ifstream in(path, std::ios::binary);
        if (!in.is_open())
            return std::string();
        in.seekg(0, in.end);
        int filesize = in.tellg();
        in.seekg(0, in.beg);

        std::string content;
        content.resize(filesize);
        in.read((char *)content.c_str(), filesize);

        in.close();
        return content;
    }

public:
    HttpServer()
    {
        _mime_type.insert(std::make_pair(".html", "text/html"));
        _mime_type.insert(std::make_pair(".default", "text/html"));
        _mime_type.insert(std::make_pair(".png", "application/x-png"));
        _mime_type.insert(std::make_pair(".gif", "image/gif"));
        _mime_type.insert(std::make_pair(".pdf", "application/pdf"));

        _code_to_desc.emplace(404, "Not Found");
        _code_to_desc.emplace(403, "Forbidden");
        _code_to_desc.emplace(302, "See Other");
        _code_to_desc.emplace(301, "Moved Permanently");
        _code_to_desc.emplace(200, "OK");
    }
    // #define TEST
    std::string HandlerHttpRequest(std::string reqmessage) // req 一定曾经被客户端序列化
    {
#ifdef TEST
        std::cout << "-------------------------------------------" << std::endl;
        std::cout << reqmessage;

        std::string respstr = "HTTP/1.1 200 OK\r\n";
        respstr += "Content-Type: text/html\r\n";
        respstr += "\r\n";
        respstr += "<html><h1>hello linux, hello world!</h1></html>";

        return respstr;
#else

        std::cout << "----------------------------------" << std::endl;
        std::cout << reqmessage;
        std::cout << std::endl;
        std::cout << "----------------------------------" << std::endl;

        HttpRequest hreq;
        HttpRespond resp;
        hreq.Deserialize(reqmessage);

        // hreq.GetMethod();
        // hreq.GetReqContent();

        if (hreq.GetPath() == "wwwroot/redir") // 跳转
        {
            std::string redir_path = "https://www.qq.com";
            resp.AddCode(302, _code_to_desc[302]);
            resp.AddHeadersKv("Location", redir_path);
        }
        else if (!hreq.GetReqContent().empty()) // 服务
        {
            if (IsServiceExists(hreq.GetPath()))
            {
                resp = _service_list[hreq.GetPath()](hreq);
            }
        }
        else
        {
            // 最基本的上层处理
            std::string content = GetFileContent(hreq.GetPath());
            if (content.empty())
            {
                content = GetFileContent("wwwroot/404.html");
                resp.AddCode(404, _code_to_desc[404]);
                resp.AddContent(content);
                resp.AddHeadersKv("Content-Length", std::to_string(content.size()));
                resp.AddHeadersKv("Content-Type", _mime_type[hreq.GetSuffix()]);
            }
            else
            {
                resp.AddCode(200, _code_to_desc[200]);
                resp.AddContent(content);
                resp.AddHeadersKv("Content-Length", std::to_string(content.size()));
                resp.AddHeadersKv("Content-Type", _mime_type[hreq.GetSuffix()]);
                resp.AddHeadersKv("Set-Cookie", "username=zhangsan");
            }
        }

        return resp.Serialize();
#endif
    }

    bool IsServiceExists(const std::string &servicename)
    {
        auto iter = _service_list.find(servicename);
        if (iter == _service_list.end())
            return false;
        else return true;
    }

    void InsertServiceList(const std::string &servicename, func_t func)
    {
        std::string s = prefixpath + servicename;
        _service_list[s] = func;
    }

    ~HttpServer() {}

private:
    std::unordered_map<std::string, std::string> _mime_type; // content-type 报头
    std::unordered_map<int, std::string> _code_to_desc;
    std::unordered_map<std::string, func_t> _service_list; // 服务列表
};

// #pragma once

// #include <iostream>
// #include <string>
// #include <vector>
// #include <sstream>
// #include <functional>
// #include <fstream>
// #include <unordered_map>

// const static std::string base_sep = "\r\n";
// const static std::string line_sep = ": ";
// const static std::string prefixpath = "wwwroot"; // web根目录
// const static std::string homepage = "index.html";
// const static std::string httpversion = "HTTP/1.0";
// const static std::string spacesep = " ";
// const static std::string suffixsep = ".";
// const static std::string html_404 = "404.html";
// const static std::string arg_sep = "?";

// using namespace log_ns;

// class HttpRequest
// {
// private:
//     // \r\n
//     // \r\ndata
//     std::string GetLine(std::string &reqstr)
//     {
//         auto pos = reqstr.find(base_sep);
//         if (pos == std::string::npos)
//             return std::string();
//         std::string line = reqstr.substr(0, pos);
//         reqstr.erase(0, line.size() + base_sep.size());
//         return line.empty() ? base_sep : line;
//     }
//     void ParseReqLine()
//     {
//         std::stringstream ss(_req_line);   // cin >>
//         ss >> _method >> _url >> _version; // /a/b/c.html or /login?user=XXX&passwd=1234 /register

//         if (strcasecmp(_method.c_str(), "GET") == 0)
//         {
//             auto pos = _url.find(arg_sep);
//             if (pos != std::string::npos)
//             {
//                 _body_text = _url.substr(pos + arg_sep.size());
//                 _url.resize(pos);
//             }
//         }

//         _path += _url;

//         if (_path[_path.size() - 1] == '/')
//         {
//             _path += homepage;
//         }

//         // wwwroot/index.html
//         // wwwroot/image/1.png
//         auto pos = _path.rfind(suffixsep);
//         if (pos != std::string::npos)
//         {
//             _suffix = _path.substr(pos);
//         }
//         else
//         {
//             _suffix = ".default";
//         }
//     }

//     void ParseReqHeader()
//     {
//         for (auto &header : _req_headers)
//         {
//             auto pos = header.find(line_sep);
//             if (pos == std::string::npos)
//                 continue;
//             std::string k = header.substr(0, pos);
//             std::string v = header.substr(pos + line_sep.size());
//             if (k.empty() || v.empty())
//                 continue;
//             _headers_kv.insert(std::make_pair(k, v));
//         }
//     }

// public:
//     HttpRequest() : _blank_line(base_sep), _path(prefixpath)
//     {
//     }
//     void Deserialize(std::string &reqstr)
//     {
//         // 基本的反序列化
//         _req_line = GetLine(reqstr);
//         std::string header;
//         do
//         {
//             header = GetLine(reqstr);
//             if (header.empty())
//                 break;
//             else if (header == base_sep)
//                 break;
//             _req_headers.push_back(header);
//         } while (true);

//         if (!reqstr.empty())
//         {
//             _body_text = reqstr;
//         }

//         // 在进一步反序列化
//         ParseReqLine();
//         ParseReqHeader();
//     }
//     std::string Url()
//     {
//         LOG(DEBUGE, "Client Want url %s\n", _url.c_str());
//         return _url;
//     }
//     std::string Path()
//     {
//         LOG(DEBUGE, "Client Want path %s\n", _path.c_str());
//         return _path;
//     }
//     std::string Suffix()
//     {
//         return _suffix;
//     }
//     std::string Method()
//     {
//         LOG(DEBUGE, "Client request method is %s\n", _method.c_str());
//         return _method;
//     }
//     std::string GetResuestBody()
//     {
//         LOG(DEBUGE, "Client request method is %s, args: %s, request path: %s\n",
//             _method.c_str(), _body_text.c_str(), _path.c_str());
//         return _body_text;
//     }
//     void Print()
//     {
//         std::cout << "----------------------------" << std::endl;
//         std::cout << "###" << _req_line << std::endl;
//         for (auto &header : _req_headers)
//         {
//             std::cout << "@@@" << header << std::endl;
//         }
//         std::cout << "***" << _blank_line;
//         std::cout << ">>>" << _body_text << std::endl;

//         std::cout << "Method: " << _method << std::endl;
//         std::cout << "Url: " << _url << std::endl;
//         std::cout << "Version: " << _version << std::endl;

//         for (auto &header_kv : _headers_kv)
//         {
//             std::cout << ")))" << header_kv.first << "->" << header_kv.second << std::endl;
//         }
//     }
//     ~HttpRequest()
//     {
//     }

// private:
//     // 基本的httprequest的格式
//     std::string _req_line;
//     std::vector<std::string> _req_headers;
//     std::string _blank_line;
//     std::string _body_text;

//     // 更具体的属性字段，需要进一步反序列化
//     std::string _method;
//     std::string _url;
//     std::string _path;
//     std::string _suffix; // 资源后缀
//     std::string _version;
//     std::unordered_map<std::string, std::string> _headers_kv;
// };

// class HttpResponse
// {
// public:
//     HttpResponse() : _verison(httpversion), _blank_line(base_sep)
//     {
//     }
//     void AddCode(int code, const std::string &desc)
//     {
//         _status_code = code;
//         _desc = desc;
//     }
//     void AddHeader(const std::string &k, const std::string &v)
//     {
//         _headers_kv[k] = v;
//     }
//     void AddBodyText(const std::string &body_text)
//     {
//         _resp_body_text = body_text;
//     }
//     std::string Serialize()
//     {
//         // 1. 构建状态行
//         _status_line = _verison + spacesep + std::to_string(_status_code) + spacesep + _desc + base_sep;

//         // 2. 构建应答报头
//         for (auto &header : _headers_kv)
//         {
//             std::string header_line = header.first + line_sep + header.second + base_sep;
//             _resp_headers.push_back(header_line);
//         }

//         // 3. 空行和正文

//         // 4. 正式序列化
//         std::string responsestr = _status_line;
//         for (auto &line : _resp_headers)
//         {
//             responsestr += line;
//         }
//         responsestr += _blank_line;
//         responsestr += _resp_body_text;

//         return responsestr;
//     }
//     ~HttpResponse()
//     {
//     }

// private:
//     // httpresponse base 属性
//     std::string _verison;
//     int _status_code;
//     std::string _desc;
//     std::unordered_map<std::string, std::string> _headers_kv;

//     // 基本的httprequest的格式
//     std::string _status_line;
//     std::vector<std::string> _resp_headers;
//     std::string _blank_line;
//     std::string _resp_body_text;
// };

// using func_t = std::function<HttpResponse(HttpRequest&)>;

// class HttpServer
// {
// private:
//     std::string GetFileContent(const std::string &path)
//     {
//         std::ifstream in(path, std::ios::binary);
//         if (!in.is_open())
//             return std::string();
//         in.seekg(0, in.end);
//         int filesize = in.tellg(); // 告知我你的rw偏移量是多少
//         in.seekg(0, in.beg);

//         std::string content;
//         content.resize(filesize);
//         in.read((char *)content.c_str(), filesize);
//         in.close();

//         return content;
//     }

// public:
//     HttpServer()
//     {
//         _mime_type.insert(std::make_pair(".html", "text/html"));
//         _mime_type.insert(std::make_pair(".jpg", "image/jpeg"));
//         _mime_type.insert(std::make_pair(".png", "image/png"));
//         _mime_type.insert(std::make_pair(".default", "text/html"));

//         _code_to_desc.insert(std::make_pair(100, "Continue"));
//         _code_to_desc.insert(std::make_pair(200, "OK"));
//         _code_to_desc.insert(std::make_pair(201, "Created"));
//         _code_to_desc.insert(std::make_pair(301, "Moved Permanently"));
//         _code_to_desc.insert(std::make_pair(302, "Found"));
//         _code_to_desc.insert(std::make_pair(404, "Not Found"));
//     }

//     // #define TEST
//     std::string HandlerHttpRequest(std::string &reqstr) // req 曾经被客户端序列化过！！！
//     {
// #ifdef TEST
//         std::cout << "---------------------------------------" << std::endl;
//         std::cout << reqstr;

//         std::string responsestr = "HTTP/1.1 200 OK\r\n";
//         responsestr += "Content-Type: text/html\r\n";
//         responsestr += "\r\n";
//         responsestr += "<html><h1>hello Linux, hello bite!</h1></html>";

//         return responsestr;
// #else
//         std::cout << "---------------------------------------" << std::endl;
//         std::cout << reqstr;
//         std::cout << "---------------------------------------" << std::endl;

//         HttpRequest req;
//         HttpResponse resp;
//         req.Deserialize(reqstr);
//         // req.Method();

//         if (req.Path() == "wwwroot/redir")
//         {
//             // 处理重定向
//             std::string redir_path = "https://www.qq.com";
//             // resp.AddCode(302, _code_to_desc[302]);
//             resp.AddCode(301, _code_to_desc[301]);
//             resp.AddHeader("Location", redir_path);
//         }
//         else if(!req.GetResuestBody().empty())
//         {
//             if(IsServiceExists(req.Path()))
//             {
//                 resp = _service_list[req.Path()](req);
//             }
//         }
//         else
//         {
//             // 最基本的上层处理,处理静态资源
//             std::string content = GetFileContent(req.Path());
//             if (content.empty())
//             {
//                 content = GetFileContent("wwwroot/404.html");
//                 resp.AddCode(404, _code_to_desc[404]);
//                 resp.AddHeader("Content-Length", std::to_string(content.size()));
//                 resp.AddHeader("Content-Type", _mime_type[".html"]);
//                 resp.AddBodyText(content);
//             }
//             else
//             {
//                 resp.AddCode(200, _code_to_desc[200]);
//                 resp.AddHeader("Content-Length", std::to_string(content.size()));
//                 resp.AddHeader("Content-Type", _mime_type[req.Suffix()]);
//                 resp.AddHeader("Set-Cookie", "username=zhangsan");
//                 // resp.AddHeader("Set-Cookie", "passwd=12345");
//                 resp.AddBodyText(content);
//             }
//         }

//         return resp.Serialize();
// #endif
//     }
//     void InsertService(const std::string &servicename, func_t f)
//     {
//         std::string s = prefixpath + servicename;
//         _service_list[s] = f;
//     }
//     bool IsServiceExists(const std::string &servicename)
//     {
//         auto iter = _service_list.find(servicename);
//         if(iter == _service_list.end()) return false;
//         else return true;
//     }
//     ~HttpServer() {}

// private:
//     std::unordered_map<std::string, std::string> _mime_type;
//     std::unordered_map<int, std::string> _code_to_desc;
//     std::unordered_map<std::string, func_t> _service_list;
// };