#pragma once
#include <unistd.h>
#include <string>
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <sys/wait.h>
#include <pthread.h>
#include <functional>
#include <iostream>

#include "Log.hpp"
#include "InetAddr.hpp"
#include "Socket.hpp"

using namespace log_ns;
using namespace socket_ns;

using service_t = std::function<std::string(std::string reqstr)>;

static const uint16_t DEF_PORT = 8888;

class TcpServer
{
public:
    TcpServer() {}
    TcpServer(service_t service, uint16_t port = DEF_PORT)
        : _port(port),
          _listensockfd(std::make_shared<TcpSocket>()),
          _isrunning(false),
          _service(service)
    {
        _listensockfd->BuildListenSocket(_port);
    }

    class ThreadData
    {
    public:
        ThreadData(SockePtr sockfd, InetAddr addr, TcpServer *self)
            : _sockfd(sockfd),
              _addr(addr),
              _self(self)
        {
        }

        ~ThreadData() {}

    public:
        SockePtr _sockfd;
        InetAddr _addr;
        TcpServer *_self;
    };

    void Loop()
    {
        _isrunning = true;
        while (_isrunning)
        {
            //  4. 获取新连接
            InetAddr client;
            SockePtr newsock = _listensockfd->Accepter(&client);
            if (nullptr == newsock)
            {
                LOG(ERROR, "Accept error!\n");
                continue;
            }

            pthread_t tid;
            ThreadData *td = new ThreadData(newsock, client, this);
            pthread_create(&tid, nullptr, Excute, td);
        }

        _isrunning = false;
    }

    static void *Excute(void *args)
    {
        pthread_detach(pthread_self()); // 线程分离
        ThreadData *td = static_cast<ThreadData *>(args);

        std::string reqmessage;
        ssize_t n = td->_sockfd->Recv(&reqmessage);
        if (n > 0)
        {
            std::string respstr = td->_self->_service(reqmessage);
            td->_sockfd->Send(respstr);
        }
        
	// close fd: test for CLOSE_WAIT
        td->_sockfd->CloseSockfd();
        delete td;

        return nullptr;
    }

    ~TcpServer()
    {
    }

private:
    uint16_t _port;
    SockePtr _listensockfd;
    bool _isrunning;
    service_t _service;
};

