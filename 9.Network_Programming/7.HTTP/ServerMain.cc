#include <iostream>
#include <memory>

#include "TcpServer.hpp"
#include "Http.hpp"

HttpRespond Login(HttpRequest &req)
{
    HttpRespond resp;
    std::cout << "外部已经拿到了参数!" << std::endl;
    req.GetReqContent();
    std::cout << "######################" << std::endl;
    resp.AddCode(200, "OK");
    resp.AddContent("<html><h1>result done!</h1></html>");

    return resp;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cerr << "udpserve argv Usage: " << argv[0] << " serverport" << std::endl;
        exit(-1);
    }

    uint16_t port = std::stoi(argv[1]);

    HttpServer http;
    http.InsertServiceList("/login", Login);

    std::unique_ptr<TcpServer> tsvr = std::make_unique<TcpServer>(
        std::bind(&HttpServer::HandlerHttpRequest, &http, std::placeholders::_1), port);

    tsvr->Loop();

    return 0;
}

// #include "TcpServer.hpp"
// #include "Http.hpp"

// HttpResponse Login(HttpRequest &req)
// {
//     HttpResponse resp;
//     std::cout << "外部已经拿到了参数了: "<< std::endl;
//     req.GetResuestBody();
//     std::cout << "####################### "<< std::endl;
//     resp.AddCode(200, "OK");
//     resp.AddBodyText("<html><h1>result done!</h1></html>");

//     // username=helloworld&userpasswd=123456

//     // 1. pipe
//     // 2. dup2
//     // 3. fork();
//     // 4. exec* -> python, PHP, 甚至是Java！

//     return resp;
// }

// // ./tcpserver 8888
// int main(int argc, char *argv[])
// {
//     if (argc != 2)
//     {
//         std::cerr << "Usage: " << argv[0] << " local-port" << std::endl;
//         exit(0);
//     }
//     uint16_t port = std::stoi(argv[1]);

//     HttpServer hserver;
//     hserver.InsertService("/login", Login);
//     // hserver.InsertService("/register", Login);
//     // hserver.InsertService("/search", Login);

//     std::unique_ptr<TcpServer> tsvr = std::make_unique<TcpServer>(
//         std::bind(&HttpServer::HandlerHttpRequest, &hserver, std::placeholders::_1), 
//         port
//     );
//     tsvr->Loop();

//     return 0;
// }