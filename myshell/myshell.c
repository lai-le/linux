#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/stat.h>     
#include <fcntl.h>

#define SIZE 512
#define NUM 50
#define SEP " "
#define ZERO '\0'
#define SkipPath(p) do{p+=strlen(p)-1;while(*p!='/') p--;}while(0)
#define SkipSpace(cmd,pos) do{\
  while(1)\
  { if(isspace(cmd[pos])) pos++;\
    else break;\
  }\
}while(0)


//"> >> <"
#define NONE_REDIR 0
#define IN_REDIR   1
#define OUT_REDIR  2
#define ADD_REDIR  3
#define IN   '<'
#define OUT  '>'
#define ADD  '>>'

char* filename = NULL;
int redir_type = NONE_REDIR;
int exitcode = 0;
char* g_Argv[NUM];
char cwd[SIZE*2];

void Die()
{
  exit(666);
}
const char* GetUserName()
{
  const char* username = getenv("LOGNAME");
  if(NULL == username)  return "none";
  return username;
}

const char* GetHostName()
{
  const char* hostname = getenv("HOSTNAME");
  if(NULL == hostname)  return "none";
  return hostname;
}

const char* Getcwd()
{
  const char* cwd = getenv("PWD"); 
  if(NULL == cwd) return "none";
  return cwd;
}

void MakeCommandLinePrint(char line[],size_t size)
{
  const char* username = GetUserName();
  const char* hostname = GetHostName();
  const char* cwd = Getcwd();

  SkipPath(cwd);
  snprintf(line,size,"[%s@%s %s]& ",username,hostname,strlen(cwd) == 1 ? "/" : cwd+1);

  printf("%s",line);
  fflush(stdout);
}

int GetUserCommand(char usercommand[],size_t size)
{

  char* s = fgets(usercommand,size,stdin);
  if(s == NULL) return -1;
  usercommand[strlen(usercommand)-1] = ZERO;
  return strlen(usercommand);
}

void SplitCommand(char command[],size_t size)
{
  (void)size;
  g_Argv[0] = strtok(command,SEP);
  int index = 1;
  while((g_Argv[index++] = strtok(NULL,SEP)));
}

void ExecuteCommand()
{
  pid_t id = fork();
  if(id < 0)  Die();
  else if(0 == id)
  {   
    //child
    if(filename != NULL)
    {
      if(redir_type == IN_REDIR)
      {
        int fd = open(filename,O_RDONLY);
        dup2(fd,0);
      }
      else if(redir_type == OUT_REDIR)
      {
        int fd = open(filename, O_CREAT | O_WRONLY | O_TRUNC, 0666);
        dup2(fd, 1);
      }
      else if(redir_type == ADD_REDIR)
      {
        int fd = open(filename, O_CREAT | O_WRONLY | O_APPEND, 0666);
        dup2(fd, 1);
      }
      else 
      {}
    }




    execvp(g_Argv[0],g_Argv);
    exit(errno);
  }
  else 
  {
    //father
    int status = 0;
    pid_t rid = waitpid(id,&status,0);
    if(rid > 0)
    {
      exitcode = WEXITSTATUS(status);
      if(exitcode != 0)
      {
        printf("%s %s:%d\n",g_Argv[0],strerror(exitcode),exitcode);
      }
    }
  }
}

const char* GetHome()
{
  const char* home = getenv("HOME");
  if(NULL == home)  return "/";
  return home;
}

void cd()
{
  const char* path = g_Argv[1];
  if(NULL == path)  path = GetHome();

  //exit
  chdir(path);
   
  char tmp[SIZE*2];
  getcwd(tmp,sizeof(tmp));
  snprintf(cwd,sizeof(cwd),"PWD=%s",tmp);
  putenv(cwd);  
}

int CheckBuildin()
{
  int yes = 0;
  const char* enter_cmd = g_Argv[0];
  if(strcmp(enter_cmd,"cd") == 0)
  {
    yes = 1;
    cd();
  }

  return yes;
}

//"ls -a -l < / >> / >"
void CheckRedirection(char cmd[])
{
  int pos = 0;
  int end = strlen(cmd);

  while(pos < end)
  {
    // in_redirection
    if(cmd[pos] == IN)  
    {
      redir_type = IN_REDIR;
      cmd[pos++] = 0;
      SkipSpace(cmd, pos);
      filename = cmd + pos;
    }
    else if(cmd[pos] == OUT)
    {
      //append_redir
      if(cmd[pos + 1] == OUT)
      {
        redir_type = ADD_REDIR;
        cmd[pos++] = 0;
        pos++;
        SkipSpace(cmd,pos);
        filename = cmd + pos;
      }//out_redir
      else
      {
        redir_type = OUT_REDIR;
        cmd[pos++] = 0;
        SkipSpace(cmd,pos);
        filename = cmd + pos;
      }
    }
    else
    {
      pos++;
    }
  }
}

int main()
{
  int quit = 0;
  while(!quit)
  {
    filename = NULL;
    redir_type = NONE_REDIR;

    //1.create a commandline
    char commandline[SIZE];
    MakeCommandLinePrint(commandline,sizeof(commandline));
  
    //2.catch user's command
    char usercommand[SIZE];
    int len = GetUserCommand(usercommand,sizeof(usercommand));
    if(len == 0)  return 1;
  
    //2.1 CheckRedirction
    CheckRedirection(usercommand);
    
    //2.2 debug
    printf("usercommand: %s\n",usercommand);
    printf("redir: %d\n",redir_type);
    printf("filename: %s\n",filename);

    //3.split command
    SplitCommand(usercommand,sizeof(usercommand));  
    
    //4.check command is in cmd 
    len = CheckBuildin();

    //5.execute command
    ExecuteCommand();
  }
  return 0;
}
